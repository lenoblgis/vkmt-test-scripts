# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class MakeAgent(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.base_url = "https://vkmt-test.lenoblgis.ru/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_make_agent(self):
        driver = self.driver
        driver.find_element_by_css_selector("div.md-toolbar-tools > button.md-button.md-ink-ripple").click()
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[1]/div[1]/textarea").clear()
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[1]/div[1]/textarea").send_keys(u"АО \"Клиника\"")
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[2]/input").clear()
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[2]/input").send_keys(u"АО \"КЛК\"")
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[3]/md-select").click()
        driver.find_element_by_xpath("/html/body/div[4]/md-select-menu/md-content/md-option[1]").click()
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container[3]/input").clear()
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container[3]/input").send_keys("clinic@mail.ru")
        driver.find_element_by_xpath("(//button[@type='button'])[8]").click()
        driver.find_element_by_css_selector("span.ng-scope").click()
        driver.find_element_by_css_selector("div.md-toolbar-tools > button.md-button.md-ink-ripple").click()
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[1]/div[1]/textarea").clear()
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[1]/div[1]/textarea").send_keys(u"АО \"Больница\"")
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[2]/input").clear()
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[2]/input").send_keys(u"АО \"БЛЦ\"")
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[3]/md-select").click()
        driver.find_element_by_xpath("/html/body/div[4]/md-select-menu/md-content/md-option[1]").click()
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container[3]/input").clear()
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container[3]/input").send_keys("bolnica@mail.ru")
        driver.find_element_by_xpath("(//button[@type='button'])[8]").click()
        driver.find_element_by_css_selector("span.ng-scope").click()
        driver.find_element_by_css_selector("div.md-toolbar-tools > button.md-button.md-ink-ripple").click()
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[1]/div[1]/textarea").clear()
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[1]/div[1]/textarea").send_keys(u"АО \"Профилакторий\"")
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[2]/input").clear()
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[2]/input").send_keys("")
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[3]/md-select").click()
        driver.find_element_by_xpath("/html/body/div[4]/md-select-menu/md-content/md-option[1]").click()
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container[3]/input").clear()
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container[3]/input").send_keys("123@mail.ru")
        driver.find_element_by_xpath("(//button[@type='button'])[8]").click()
        driver.find_element_by_css_selector("span.ng-scope").click()

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
