# -*- coding: utf-8 -*-
'''
@author: smirnov
'''
import unittest
from selenium import webdriver
from AccountPage import LoginPage
from ContactPage import ContactPage
from CompanyPage import CompanySection
import xlrd
import random

class TestContactSection(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://vkmt-test.lenoblgis.ru/#/login')
        self.driver.implicitly_wait(5)
        self.driver.maximize_window() # max browser window size
        
    def tearDown(self):
        self.driver.close()
           
    def testContactCreation(self):
        loginPage = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
        cmn = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Contact_DDT.xlsx')
        wb2 = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        sheet2 = wb2.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        #for i in range(12):
        rownum = (1)
        rows = sheet1.row_values(rownum)
        rows1 = sheet2.row_values(rownum)
        cmn.creat_company(rows1[0], rows1[1], rows1[3], rows1[5], rows1[6], rows1[7], rows1[8], rows1[9], rows1[10], rows1[11], rows1[12], rows1[13], rows1[14], rows1[15], rows1[16], rows1[17], rows1[18])
        cnt.creat_contact(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7], CompanyShortName = rows1[1])
        cnt.assert_contact_info(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7], CompanyName = rows1[0])
        
        cnt.delete_contact_after_test()
        cnt.delete_company_after_test()
        
    def testContactCreationViaCompany(self):
        lgn = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
        cmn = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Contact_DDT.xlsx')
        wb2 = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        sheet2 = wb2.sheet_by_index(0)
        
        lgn.enter_username_password('admin', '123')
        
        rownum = 2
        rows = sheet1.row_values(rownum)
        rows1 = sheet2.row_values(rownum)
        cmn.creat_company(rows1[0], rows1[1], rows1[3], rows1[5], rows1[6], rows1[7], rows1[8], rows1[9], rows1[10], rows1[11], rows1[12], rows1[13], rows1[14], rows1[15], rows1[16], rows1[17], rows1[18])
        cnt.creat_contact_via_company(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6])
        cnt.assert_contact_info(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[8])
        
        cnt.delete_contact_after_test()
        cnt.delete_company_after_test()
        
    def testContactModification(self):
        loginPage = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Contact_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 1
        rows = sheet1.row_values(rownum)
        cnt.creat_contact(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7])
        cnt.assert_contact_info(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7])
        
        rownum = 12
        rows = sheet1.row_values(rownum)
        cnt.modify_contact(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7])
        cnt.assert_contact_info(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7])
        
        cnt.delete_contact_after_test() 
        
    def testContactVersionCreation(self):
        loginPage = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
        companyPage = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Contact_DDT.xlsx')
        wb2 = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        sheet2 = wb2.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 1
        rows = sheet1.row_values(rownum)
        rows1 = sheet2.row_values(rownum)
        companyPage.creat_company(rows1[0], rows1[1], rows1[3], rows1[5], rows1[6], rows1[7], rows1[8], rows1[9], rows1[10], rows1[11], rows1[12], rows1[13], rows1[14], rows1[15], rows1[16], rows1[17], rows1[18])
        cnt.creat_contact(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7], CompanyShortName = rows1[1])
        cnt.assert_contact_info(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7], CompanyName =  rows1[0])
        
        rownum = 2
        rows = sheet1.row_values(rownum)
        cnt.modify_contact(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7])
        
        rownum = 1
        rows = sheet1.row_values(rownum)
        cnt.assert_contact_version_info(rows[0], rows[1], rows[2], rows1[0], rows[3], rows[4], rows[5], rows[6], rows[7])
        
        cnt.delete_contact_after_test()
        cnt.delete_company_after_test()
    
    def testRecoverContactVersion(self):
        loginPage = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
        companyPage = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Contact_DDT.xlsx')
        wb2 = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        sheet2 = wb2.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 3
        rows = sheet1.row_values(rownum)
        rows1 = sheet2.row_values(rownum)
        companyPage.creat_company(rows1[0], rows1[1], rows1[3], rows1[5], rows1[6], rows1[7], rows1[8], rows1[9], rows1[10], rows1[11], rows1[12], rows1[13], rows1[14], rows1[15], rows1[16], rows1[17], rows1[18])
        cnt.creat_contact(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7], CompanyShortName = rows1[1])
        
        rownum = 2
        rows = sheet1.row_values(rownum)
        cnt.modify_contact(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7])
        
        rownum = 3
        rows = sheet1.row_values(rownum)
        cnt.contact_version_recovery()
        cnt.assert_contact_info(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7], CompanyName = rows1[0])
        
        cnt.delete_contact_after_test()
        cnt.delete_company_after_test()
        
    def testContactDeleting(self):
        loginPage = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
        companyPage = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Contact_DDT.xlsx')
        wb2 = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        sheet2 = wb2.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 8
        rows = sheet1.row_values(rownum)
        rows1 = sheet2.row_values(rownum)
        companyPage.creat_company(rows1[0], rows1[1], rows1[3], rows1[5], rows1[6], rows1[7], rows1[8], rows1[9], rows1[10], rows1[11], rows1[12], rows1[13], rows1[14], rows1[15], rows1[16], rows1[17], rows1[18])
        cnt.creat_contact(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7], CompanyShortName = rows1[1])
        cnt.delete_contact(rows[0])
        cnt.assert_deleted_contact(rows[0], rows[1], rows[2], rows1[0], rows[3], rows[4], rows[5], rows[6], rows[7])
        
        cnt.delete_contact_after_test()
        cnt.delete_company_after_test()
        
    def testContextContactSearchByLastName(self):
        loginPage = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Contact_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 1
        rows = sheet1.row_values(rownum)
        cnt.creat_contact(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7])
        cnt.search_contact_by_fields(rows[0], rows[0]) #rows[0] is a LastName
        
        cnt.delete_contact_after_test()
        
    def testContextContactSearchByFirstName(self):
        loginPage = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Contact_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 4
        rows = sheet1.row_values(rownum)
        cnt.creat_contact(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7])
        cnt.search_contact_by_fields(rows[1], rows[1]) #rows[1] is a FirstName
        
        cnt.delete_contact_after_test()
        
    def testContextContactSearchByMiddleName(self):
        loginPage = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Contact_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 5
        rows = sheet1.row_values(rownum)
        cnt.creat_contact(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7])
        cnt.search_contact_by_fields(rows[2], rows[2]) #rows[2] is a MiddleName
        
        cnt.delete_contact_after_test()
        
    def testContextContactSearchByCompanyShortName(self):
        loginPage = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
        companyPage = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Contact_DDT.xlsx')
        wb2 = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        sheet2 = wb2.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 5
        rows = sheet1.row_values(rownum)
        rows1 = sheet2.row_values(rownum)
        companyPage.creat_company(rows1[0], rows1[1], rows1[3], rows1[5], rows1[6], rows1[7], rows1[8], rows1[9], rows1[10], rows1[11], rows1[12], rows1[13], rows1[14], rows1[15], rows1[16], rows1[17], rows1[18])
        cnt.creat_contact(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7], CompanyShortName = rows1[1])
        cnt.search_contact_by_fields(rows1[1], rows[0]) #rows1[1] is a Contact ShortCompany Name (for Searching); rows[0] is a Contact LastName (for asserting)
        
        cnt.delete_contact_after_test()
        cnt.delete_company_after_test()
        
    def testContextContactSearchByCompanyName(self):
        loginPage = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
        companyPage = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Contact_DDT.xlsx')
        wb2 = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        sheet2 = wb2.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 7
        rows = sheet1.row_values(rownum)
        rows1 = sheet2.row_values(rownum)
        companyPage.creat_company(rows1[0], rows1[1], rows1[3], rows1[5], rows1[6], rows1[7], rows1[8], rows1[9], rows1[10], rows1[11], rows1[12], rows1[13], rows1[14], rows1[15], rows1[16], rows1[17], rows1[18])
        cnt.creat_contact(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7], CompanyShortName = rows1[1])
        cnt.search_contact_by_fields(rows1[0], rows[0]) #rows1[] is a Contact Company Name (for Searching); rows[0] is a Contact LastName (for asserting)
        
        cnt.delete_contact_after_test()
        cnt.delete_company_after_test()
    
    def testContextContactSearchByPosition(self):
        loginPage = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Contact_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 8
        rows = sheet1.row_values(rownum)
        cnt.creat_contact(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7])
        cnt.search_contact_by_fields(rows[3], rows[0]) #rows[3] is a position; rows[0] is a Contact LastName (for asserting)
        
        cnt.delete_contact_after_test()
            
    def testMaxSymbolsInContactCreationForm(self):
        loginPage = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
       
        loginPage.enter_username_password('admin', '123')
        
        print '\nFor 50, 50, 50, 255, 255: \n _______________'
        cnt.fill_contact_creation_form('L' * 50, 'F' * 50, 'M' * 50, 'P' * 255, 'A' * 255)
        cnt.assert_overlong_symbols_error_present()
        
    def testMaxPlus1SymbolsInContactCreationForm(self):
        loginPage = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
       
        loginPage.enter_username_password('admin', '123')
        
        print '\nFor 51, 51, 51, 256, 256: \n _______________'
        cnt.fill_contact_creation_form('L' * 51, 'F' * 51, 'M' * 51, 'P' * 256, 'A' * 256)
        cnt.assert_overlong_symbols_error_present()
        
    def testMaxMinus1SymbolsInContactCreationForm(self):
        loginPage = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
       
        loginPage.enter_username_password('admin', '123')
        
        print '\nFor 49, 49, 49, 254, 254: \n _______________'
        cnt.fill_contact_creation_form('L' * 49, 'F' * 49, 'M' * 49, 'P' * 254, 'A' * 254)
        cnt.assert_overlong_symbols_error_present()
        
    def testZeroSymbolsInContactCreationForm(self):
        loginPage = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
       
        loginPage.enter_username_password('admin', '123')
        
        print '\nFor 0, 0, 0, 0, 0: \n _______________'
        cnt.fill_contact_creation_form('L' * 0, 'F' * 0, 'M' * 0, 'P' * 0, 'A' * 0)
        cnt.assert_overlong_symbols_error_present()
        
    def testContactCreationForDB(self):
        loginPage = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
       
        loginPage.enter_username_password('admin', '123')
        
        for i in range(250):
            SecondName = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(8))
            FirstName = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(8))
            MiddleName = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(8))
            Possition = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(8))
            PersonalPhone = '+7 999 999 99 99'
            OfficialPhone = '+7 999 999 99 99'
            Email = '123@mail.ru'
            Address = 'www.123.ru'
            cnt.creat_contact(SecondName, FirstName, MiddleName, Possition, PersonalPhone, OfficialPhone, Email, Address)
            
    def testContactCreationForDemo(self):
        loginPage = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
        #cmn = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Contact_DDT.xlsx')
        wb2 = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        sheet2 = wb2.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        for i in range(12):
            rownum = (i)
            rows = sheet1.row_values(rownum)
            rows1 = sheet2.row_values(rownum)
            #cmn.creat_company(rows1[0], rows1[1], rows1[3], rows1[5], rows1[6], rows1[7], rows1[8], rows1[9], rows1[10], rows1[11], rows1[12], rows1[13], rows1[14], rows1[15], rows1[16], rows1[17], rows1[18])
            cnt.creat_contact(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7], CompanyShortName = rows1[1])
            
            
        
        
        
        
                
        
        
        
        
        