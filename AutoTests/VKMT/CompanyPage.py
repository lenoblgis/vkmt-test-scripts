# -*- coding: utf-8 -*-
'''
@author: smirnov
'''

from locators import CompanySectionLocators, TrashSectionLocators
import time
import xlrd

class CompanySection():
    
    def __init__(self, driver):
        self.driver = driver
        
    def choice_company_type(self, CompanyType):
        self.driver.find_element(*CompanySectionLocators.companyTypeSelectButton).click()
        time.sleep(0.3)
        self.driver.find_element_by_xpath('/html/body/div[5]/md-select-menu/md-content/md-option[%s]' % CompanyType).click()
        time.sleep(0.3)
        
    def choice_company_kind(self, Kind, Kind2):
        self.driver.find_element(*CompanySectionLocators.companyKindSelectButton).click()
        time.sleep(0.3)
        self.driver.find_element_by_xpath('/html/body/div[6]/md-select-menu/md-content/md-optgroup[%s]/md-option[%s]' % (Kind, Kind2)).click()
        time.sleep(0.3)
        
    def creat_company(self, Name, ShortName, CompanyType, Kind, Kind2, Phone, Fax, Email, Site, SubjectAdress, DistrictAdress, CityAdress, Street, House, Building, Housing, Room):
        time.sleep(1)
        self.driver.find_element(*CompanySectionLocators.companySectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.creatCompanyButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyNameField).send_keys(Name)
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyShortNameField).send_keys(ShortName)
        time.sleep(0.5)
        CompanySection.choice_company_type(self, CompanyType)
        time.sleep(0.5)
        if Kind:
            CompanySection.choice_company_kind(self, Kind, Kind2)
        self.driver.find_element(*CompanySectionLocators.companyPhoneField).send_keys(Phone)
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyFaxField).send_keys(Fax)
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyEmailField).send_keys(Email)
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companySiteField).send_keys(Site)
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.nextPageInCompanyCreationFormButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companySubjectAdressField).click()
        self.driver.find_element(*CompanySectionLocators.companySubjectAdressField).send_keys(SubjectAdress)
        time.sleep(0.5)
        if DistrictAdress:
            self.driver.find_element(*CompanySectionLocators.companyDistrictAdressField).click()
            self.driver.find_element(*CompanySectionLocators.companyDistrictAdressField).send_keys(DistrictAdress)
            time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyCityAdressField).click()
        self.driver.find_element(*CompanySectionLocators.companyCityAdressField).send_keys(CityAdress)
        time.sleep(0.5)
        if Street: self.driver.find_element(*CompanySectionLocators.companyStreetField).send_keys(Street) 
        if House: self.driver.find_element(*CompanySectionLocators.companyHouseField).send_keys(int(House))  
        if Building: self.driver.find_element(*CompanySectionLocators.companyBuildingField).send_keys(int(Building))       
        if Housing: self.driver.find_element(*CompanySectionLocators.companyHousingField).send_keys(int(Housing))   
        if Room: self.driver.find_element(*CompanySectionLocators.companyRoomField).send_keys(int(Room))  
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.saveCompanyDataButton).click()
        time.sleep(1)
        
    def modify_company(self, Name, ShortName, CompanyType, Kind, Kind2, Phone, Fax, Email, Site, SubjectAdress, DistrictAdress, CityAdress, Street, House, Building, Housing, Room):
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyModificationButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyNameField).clear()
        self.driver.find_element(*CompanySectionLocators.companyNameField).send_keys(Name)
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyShortNameField).clear()
        self.driver.find_element(*CompanySectionLocators.companyShortNameField).send_keys(ShortName)
        time.sleep(0.5)
        CompanySection.choice_company_type(self, CompanyType)
        time.sleep(0.5)
        if Kind:
            CompanySection.choice_company_kind(self, Kind, Kind2)
        self.driver.find_element(*CompanySectionLocators.companyPhoneField).clear()
        self.driver.find_element(*CompanySectionLocators.companyPhoneField).send_keys(Phone)
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyFaxField).clear()
        self.driver.find_element(*CompanySectionLocators.companyFaxField).send_keys(Fax)
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyEmailField).clear()
        self.driver.find_element(*CompanySectionLocators.companyEmailField).send_keys(Email)
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companySiteField).clear()
        self.driver.find_element(*CompanySectionLocators.companySiteField).send_keys(Site)
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.nextPageInCompanyCreationFormButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.clearCompanySubjectAdressFieldButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companySubjectAdressField).click()
        self.driver.find_element(*CompanySectionLocators.companySubjectAdressField).send_keys(SubjectAdress)
        time.sleep(0.5)
        if DistrictAdress:
            self.driver.find_element(*CompanySectionLocators.companyDistrictAdressField).click()
            self.driver.find_element(*CompanySectionLocators.companyDistrictAdressField).send_keys(DistrictAdress)
            time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyCityAdressField).click()
        self.driver.find_element(*CompanySectionLocators.companyCityAdressField).send_keys(CityAdress)
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyStreetField).clear()
        if Street: self.driver.find_element(*CompanySectionLocators.companyStreetField).send_keys(Street) 
        self.driver.find_element(*CompanySectionLocators.companyHouseField).clear()
        if House: self.driver.find_element(*CompanySectionLocators.companyHouseField).send_keys(int(House))  
        self.driver.find_element(*CompanySectionLocators.companyBuildingField).clear()
        if Building: self.driver.find_element(*CompanySectionLocators.companyBuildingField).send_keys(int(Building))  
        self.driver.find_element(*CompanySectionLocators.companyHousingField).clear()     
        if Housing: self.driver.find_element(*CompanySectionLocators.companyHousingField).send_keys(int(Housing)) 
        self.driver.find_element(*CompanySectionLocators.companyRoomField).clear()  
        if Room: self.driver.find_element(*CompanySectionLocators.companyRoomField).send_keys(int(Room))   
        self.driver.find_element(*CompanySectionLocators.saveCompanyDataButton).click()
        time.sleep(1)
        
    def modify_short_company(self, Name, ShortName): 
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyModificationButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyNameField).clear()
        self.driver.find_element(*CompanySectionLocators.companyNameField).send_keys(Name)
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyShortNameField).clear()
        self.driver.find_element(*CompanySectionLocators.companyShortNameField).send_keys(ShortName)
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.nextPageInCompanyCreationFormButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.saveCompanyDataButton).click()
        time.sleep(1)
        
                   
    def assert_company_info(self, rownum):
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        rows = sheet1.row_values(rownum)
        
        assert rows[0] in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        assert rows[1] in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        assert rows[2] in self.driver.find_element(*CompanySectionLocators.companyInfo).text 
        if rows[4]: assert rows[4] in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if rows[7]: assert rows[7] in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if rows[8]: assert rows[8] in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if rows[9]: assert rows[9] in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if rows[10]: assert rows[10] in self.driver.find_element(*CompanySectionLocators.companyInfo).text    
        if rows[11]: assert rows[11] in self.driver.find_element(*CompanySectionLocators.companyInfo).text    
        if rows[12]: assert rows[12] in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if rows[13]: assert rows[13] in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if rows[14]: assert rows[14] in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if rows[15]: assert rows[15] in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if rows[16]: assert rows[16] in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if rows[17]: assert rows[17] in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if rows[18]: assert rows[18] in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        
    def delete_company_from_companycard(self):
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyDeleteButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.deleteConfirmationButton).click()
        time.sleep(0.5)
        
    def delete_company(self, Name):
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companySectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.contextSearchField).send_keys(Name)
        time.sleep(2)
        self.driver.find_element(*CompanySectionLocators.firstStringInList).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyDeleteButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.deleteConfirmationButton).click()
        time.sleep(0.5)
        
    def search_company_by_field(self, FieldForSearch, ShortNameFieldForAsserting):
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companySectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.contextSearchField).send_keys(FieldForSearch)
        time.sleep(2)
        assert ShortNameFieldForAsserting in self.driver.find_element(*CompanySectionLocators.firstStringInList).text
        
    def assert_company_version_creation(self):
        assert u'2 (текущая)' in self.driver.find_element(*CompanySectionLocators.listOfVersion).text
        assert '1' in self.driver.find_element(*CompanySectionLocators.listOfVersion).text
        time.sleep(0.5)
        
    def assert_company_version_info(self, Name, ShortName, CompanyType, Kind, Phone, Fax, Email, Site, SubjectAdress, DistrictAdress, CityAdress, Street, House, Building, Housing, Room):
        self.driver.find_element(*CompanySectionLocators.versionSelector).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.previousVersionButton).click()
        time.sleep(0.5)
        assert Name in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        assert ShortName in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        assert CompanyType in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if Kind: assert Kind in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if Phone: assert Phone in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if Fax: assert Fax in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if Email: assert Email in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if Site: assert Site in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if SubjectAdress: assert SubjectAdress in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if DistrictAdress: assert DistrictAdress in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if CityAdress: assert CityAdress in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if Street: assert Street in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if House: assert House in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if Building: assert Building in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if Housing: assert Housing in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        if Room: assert Room in self.driver.find_element(*CompanySectionLocators.companyInfo).text
        
    def company_version_recovery(self):
        self.driver.find_element(*CompanySectionLocators.versionSelector).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.previousVersionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyVersionRecoveryButton).click()
        time.sleep(0.5)
        
    def fill_company_creation_form(self, Name, ShortName, Site):
        self.driver.find_element(*CompanySectionLocators.companySectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.creatCompanyButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyNameField).send_keys(Name)
        self.driver.find_element(*CompanySectionLocators.companyShortNameField).send_keys(ShortName)
        self.driver.find_element(*CompanySectionLocators.companySiteField).send_keys(Site)
        self.driver.find_element(*CompanySectionLocators.companyPhoneField).click()
        time.sleep(0.5)
        
    def assert_overlong_symbols_error_present(self):
        try:
            assert u'Слишком длинно.' in self.driver.find_element(*CompanySectionLocators.overlongNameError).text
            print 'Error for Name symbols'
        except:
            print 'for Name symbols is OK'
        try:
            assert u'Слишком длинно.' in self.driver.find_element(*CompanySectionLocators.overlongShortNameError).text
            print 'Error for ShortName symbols'
        except:
            print 'for ShortName symbols is OK'
        try:
            assert u'Слишком длинно.' in self.driver.find_element(*CompanySectionLocators.overlongSiteError).text
            print 'Error for Site symbols'
        except:
            print 'for Site symbols is OK'
            
    def recover_deleted_company(self, ShotName):
        self.driver.find_element(*TrashSectionLocators.trashSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*TrashSectionLocators.recoverCompanyTrashButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companySectionButton).click()
        time.sleep(0.5)
        self.driver.find_element_by_xpath("//*[contains(text(), '%s')]" % ShotName).click()
        time.sleep(0.5)
    
    def assert_deleted_company(self, Name, ShortName, CompanyType, Kind, Phone, Fax, Email, Site):
        self.driver.find_element(*TrashSectionLocators.trashSectionButton).click()
        time.sleep(0.5)
        print ShortName
        self.driver.find_element_by_class_name('"wrap ng-binding">Минздрав России (Государственные органы власти)<').click()
        time.sleep(0.5)
        if Name: assert Name in self.driver.find_element(*TrashSectionLocators.companyTrashInfo).text
        if ShortName: assert ShortName in self.driver.find_element(*TrashSectionLocators.companyTrashInfo).text
        if CompanyType: assert CompanyType in self.driver.find_element(*TrashSectionLocators.companyTrashInfo).text
        if Kind: assert Kind in self.driver.find_element(*TrashSectionLocators.companyTrashInfo).text 
        if Phone: assert Phone in self.driver.find_element(*TrashSectionLocators.companyTrashInfo).text 
        if Fax: assert Fax in self.driver.find_element(*TrashSectionLocators.companyTrashInfo).text 
        if Email: assert Email in self.driver.find_element(*TrashSectionLocators.companyTrashInfo).text 
        if Site: assert Site in self.driver.find_element(*TrashSectionLocators.companyTrashInfo).text 
        
    def delete_company_after_test(self):
        self.driver.find_element(*CompanySectionLocators.companySectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.firstStringInList).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyDeleteButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.deleteConfirmationButton).click()
        time.sleep(0.5)
        
    def create_short_company(self, Name, ShortName):
        time.sleep(1)
        self.driver.find_element(*CompanySectionLocators.companySectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.creatCompanyButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyNameField).send_keys(Name)
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyShortNameField).send_keys(ShortName)
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.nextPageInCompanyCreationFormButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.saveCompanyDataButton).click()
        time.sleep(1)
        
        
        