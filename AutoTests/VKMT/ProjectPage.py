# -*- coding: utf-8 -*-
'''
@author: smirnov
'''

from locators import ProjectPageLocators, AccountSectionLocators, CompanySectionLocators
import time
from selenium.webdriver.support.ui import Select

class ProjectPage():
    
    def __init__(self, driver):
        self.driver = driver
        
    def create_project(self, Name, CompanyShortName, PartnerShortName, Type, Status, RealizationDate, FinancingSourse, EstimateBudget, ProjectContext):
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.projectSectionButton).click() 
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.createProjectButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.projectNameField).send_keys(Name)
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.projectCompanySelector).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.companySearchFieldInProject).send_keys(CompanyShortName)
        time.sleep(1)
        try: self.driver.find_element(*ProjectPageLocators.firstStringInCompanySearchResult).click()
        except: self.driver.find_element(*ProjectPageLocators.firstStringInCompanySearchResultWhileModifying).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.showAdditionFieldButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.projectParticipantSelector).click()
        time.sleep(0.5)
        if PartnerShortName:
            self.driver.find_element(*ProjectPageLocators.participantSearchFieldInProject).send_keys(PartnerShortName)
            time.sleep(0.5)
        try: self.driver.find_element(*ProjectPageLocators.firstStringInPartnerSearchResultWhileModifying).click()
        except: self.driver.find_element(*ProjectPageLocators.firstStringInPartnerSearchResult).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.typeSelector).click()
        time.sleep(0.5)
        try: self.driver.find_element_by_xpath('/html/body/div[6]/md-select-menu/md-content/md-option[%s]' % Type).click()
        except: self.driver.find_element_by_xpath('/html/body/div[5]/md-select-menu/md-content/md-option[%s]' % Type).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.statusSelector).click()
        time.sleep(0.5)
        try: self.driver.find_element_by_xpath('/html/body/div[7]/md-select-menu/md-content/md-option[%s]' % Status).click()
        except: self.driver.find_element_by_xpath('/html/body/div[6]/md-select-menu/md-content/md-option[%s]' % Status).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.realizationDateField).send_keys(RealizationDate)
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.financingSourseButton).click()
        time.sleep(0.5)
        self.driver.find_element_by_xpath('/html/body/div[8]/md-select-menu/md-content/md-option[%s]' % FinancingSourse).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.estimateBudgetField).send_keys(EstimateBudget)
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.projectContextField).send_keys(ProjectContext)
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.saveProjectDataButton).click()
        time.sleep(1)
        
    def add_atachment_to_the_project(self, ProjectName, PathToFile):
        self.driver.find_element(*ProjectPageLocators.projectSectionButton).click() 
        time.sleep(0.5)
        self.driver.find_element_by_xpath("//*[contains(text(), '%s')]" % ProjectName).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.projectAttachmentButton).click()
        time.sleep(0.8)
        self.driver.find_element_by_css_selector("input[type=\"file\"]").send_keys(PathToFile)
        time.sleep(30)
        
    def assert_project_info_in_card(self, Name, CompanyShortName, PartnerShortName, Type, Status, RealizationDate, FinancingSourse, EstimateBudget, ProjectContext):
        time.sleep(1.5)
        assert Name in self.driver.find_element(*ProjectPageLocators.projectInfoTitle).text
        assert CompanyShortName in self.driver.find_element(*ProjectPageLocators.projectInfoView).text
        if PartnerShortName: assert PartnerShortName in self.driver.find_element(*ProjectPageLocators.projectInfoView).text
        assert Type in self.driver.find_element(*ProjectPageLocators.projectInfoView).text
        assert Status in self.driver.find_element(*ProjectPageLocators.projectInfoView).text
        assert RealizationDate in self.driver.find_element(*ProjectPageLocators.projectInfoView).text
        assert FinancingSourse in self.driver.find_element(*ProjectPageLocators.projectInfoView).text
        assert EstimateBudget in self.driver.find_element(*ProjectPageLocators.projectInfoView).text
        assert ProjectContext in self.driver.find_element(*ProjectPageLocators.projectInfoView).text
        time.sleep(0.5)
        
    def modify_project(self, Name, CompanyShortName, PartnerShortName, Type, Status, RealizationDate, FinancingSourse, EstimateBudget, ProjectContext):
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.modifyProjectButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.projectNameField).clear()
        self.driver.find_element(*ProjectPageLocators.projectNameField).send_keys(Name)
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.projectCompanySelector).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.companySearchFieldInProject).send_keys(CompanyShortName)
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.firstStringInCompanySearchResultWhileModifying).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.projectParticipantSelector).click()
        time.sleep(0.5)
        if PartnerShortName:
            self.driver.find_element(*ProjectPageLocators.participantSearchFieldInProject).send_keys(PartnerShortName)
            time.sleep(0.5)
        self.driver.find_element_by_xpath('/html/body/div[6]/md-select-menu/md-content/md-option').click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.typeSelector).click()
        time.sleep(0.5)
        self.driver.find_element_by_xpath('/html/body/div[7]/md-select-menu/md-content/md-option[%s]' % Type).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.statusSelector).click()
        time.sleep(0.5)
        self.driver.find_element_by_xpath('/html/body/div[8]/md-select-menu/md-content/md-option[%s]' % Status).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.realizationDateField).clear()
        self.driver.find_element(*ProjectPageLocators.realizationDateField).send_keys(RealizationDate)
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.financingSourseButton).click()
        time.sleep(0.5)
        self.driver.find_element_by_xpath('/html/body/div[9]/md-select-menu/md-content/md-option[%s]' % FinancingSourse).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.estimateBudgetField).clear()
        self.driver.find_element(*ProjectPageLocators.estimateBudgetField).send_keys(EstimateBudget)
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.projectContextField).clear()
        self.driver.find_element(*ProjectPageLocators.projectContextField).send_keys(ProjectContext)
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.saveProjectDataButtonWhileModifying).click()
        time.sleep(1)
        
    def delete_project(self):
        time.sleep(1)
        self.driver.find_element(*ProjectPageLocators.deleteProjectButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.deleteConfirmationButton).click()
        time.sleep(1)
        
    def assert_if_project_deleted(self, ProjectName):
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.projectSectionButton).click() 
        time.sleep(0.5)
        try: assert ProjectName in self.driver.find_element(*ProjectPageLocators.projectList).text
        except: print 'For testProjectDeleting: project has been removed'
        
    def delete_project_after_test(self):
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.projectSectionButton).click() 
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.firstProjectInList).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.deleteProjectButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.deleteConfirmationButton).click()
        
    def search_project_by_field(self, FieldForSearch, FieldForAssert):
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.projectSectionButton).click() 
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.contextSearchField).send_keys(FieldForSearch)
        time.sleep(2)
        assert FieldForAssert in self.driver.find_element(*ProjectPageLocators.firstProjectInList).text
        time.sleep(0.5)
        
    def fill_project_creation_form(self, Name, RegistryNumber, RealizationDate, FinancingSourse, EstimateBudget, ProjectContext):
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.projectSectionButton).click() 
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.createProjectButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.showAdditionFieldButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.projectNameField).send_keys(Name)
        self.driver.find_element(*ProjectPageLocators.registryNumberField).send_keys(RegistryNumber)
        self.driver.find_element(*ProjectPageLocators.realizationDateField).send_keys(RealizationDate)
        self.driver.find_element(*ProjectPageLocators.financingSourseField).send_keys(FinancingSourse)
        self.driver.find_element(*ProjectPageLocators.estimateBudgetField).send_keys(EstimateBudget)
        self.driver.find_element(*ProjectPageLocators.projectContextField).send_keys(ProjectContext)
        self.driver.find_element(*ProjectPageLocators.estimateBudgetField).click()
        
    def assert_overlong_symbols_error_present(self):
        try:
            assert u'Слишком длинно.' in self.driver.find_element(*ProjectPageLocators.overLongNameError).text
            print 'Error for Name symbols'
        except:
            print 'for Name symbols is OK'
            
        try:
            assert u'Слишком длинно.' in self.driver.find_element(*ProjectPageLocators.overLongRegistryNumberError).text
            print 'Error for RegistryNumber symbols'
        except:
            print 'for RegistryNumber symbols is OK'
            
        try:
            assert u'Слишком длинно.' in self.driver.find_element(*ProjectPageLocators.overLongRealizationDateError).text
            print 'Error for RealizationDate symbols'
        except:
            print 'for RealizationDate symbols is OK'
            
        try:
            assert u'Слишком длинно.' in self.driver.find_element(*ProjectPageLocators.overLongFinancingSourseError).text
            print 'Error for FinancingSourse symbols'
        except:
            print 'for FinancingSourse symbols is OK'
            
        try:
            assert u'Слишком длинно.' in self.driver.find_element(*ProjectPageLocators.overLongEstimateBudgetError).text
            print 'Error for EstimateBudget symbols'
        except:
            print 'for EstimateBudget symbols is OK'
            
        try:
            assert u'Слишком длинно.' in self.driver.find_element(*ProjectPageLocators.overLongProjectContextError).text
            print 'Error for ProjectContext symbols'
        except:
            print 'for ProjectContext symbols is OK'  
            
            
    def open_previous_version(self):
        time.sleep(1)
        self.driver.find_element(*ProjectPageLocators.versionSelector).click()
        time.sleep(0.5)
        self.driver.find_element(*ProjectPageLocators.previousVersion).click()
        
    def recover_previous_version(self):
        self.driver.find_element(*ProjectPageLocators.versionRecoverButton).click()
        time.sleep(1)
        
        
            
        
        
        
        
        
        
        
        
        