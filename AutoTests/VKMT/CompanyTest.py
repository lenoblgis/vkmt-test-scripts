# -*- coding: utf-8 -*-
'''
@author: smirnov
'''
import unittest
from selenium import webdriver
from AccountPage import LoginPage
from CompanyPage import CompanySection
import xlrd
import random

class TestCompanySection(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://vkmt-test.lenoblgis.ru/#/login')
        self.driver.implicitly_wait(5)
        self.driver.maximize_window() # max browser window size
        
    def tearDown(self):
        self.driver.close()

    def testCompanyCreation(self):
        loginPage = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        #for i in range(12):
        rownum = (1)
        rows = sheet1.row_values(rownum)
        cmn.creat_company(rows[0], rows[1], rows[3], rows[5], rows[6], rows[7], rows[8], rows[9], rows[10], rows[11], rows[12], rows[13], rows[14], rows[15], rows[16], rows[17], rows[18])
        cmn.assert_company_info(rownum)
            
        cmn.delete_company_after_test()
        
    def testCompanyModification(self):
        loginPage = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        rownum = 1
        rows = sheet1.row_values(rownum)
        
        loginPage.enter_username_password('admin', '123')
        
        cmn.creat_company(rows[0], rows[1], rows[3], rows[5], rows[6], rows[7], rows[8], rows[9], rows[10], rows[11], rows[12], rows[13], rows[14], rows[15], rows[16], rows[17], rows[18])
        
        rownum = 7
        rows = sheet1.row_values(rownum)
        
        cmn.modify_company(rows[0], rows[1], rows[3], rows[5], rows[6], rows[7], rows[8], rows[9], rows[10], rows[11], rows[12], rows[13], rows[14], rows[15], rows[16], rows[17], rows[18])
        cmn.assert_company_info(rownum)
        
        cmn.delete_company_after_test()
     
    def testCompanyDeleting(self):
        loginPage = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        rownum = 4
        rows = sheet1.row_values(rownum)
        
        loginPage.enter_username_password('admin', '123')
        
        cmn.creat_company(rows[0], rows[1], rows[3], rows[5], rows[6], rows[7], rows[8], rows[9], rows[10], rows[11], rows[12], rows[13], rows[14], rows[15], rows[16], rows[17], rows[18])
        cmn.delete_company(rows[0])
         
    def testContextCompanySearchByName(self): 
        loginPage = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        rownum = 3
        rows = sheet1.row_values(rownum)
        
        loginPage.enter_username_password('admin', '123')
        
        cmn.creat_company(rows[0], rows[1], rows[3], rows[5], rows[6], rows[7], rows[8], rows[9], rows[10], rows[11], rows[12], rows[13], rows[14], rows[15], rows[16], rows[17], rows[18])
        cmn.search_company_by_field(rows[0], rows[1]) #rows[0] is a Company Name (for searching); rows[1] is a ShortName (for asserting)
        
        cmn.delete_company_after_test()
        
    def testContextCompanySearchByShortName(self): 
        loginPage = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        rownum = 4
        rows = sheet1.row_values(rownum)
        
        loginPage.enter_username_password('admin', '123')
        
        cmn.creat_company(rows[0], rows[1], rows[3], rows[5], rows[6], rows[7], rows[8], rows[9], rows[10], rows[11], rows[12], rows[13], rows[14], rows[15], rows[16], rows[17], rows[18])
        cmn.search_company_by_field(rows[1], rows[1]) #rows[1] is a ShortName (for searching and asserting)
        
        cmn.delete_company_after_test()
        
    def testCompanyVersionCreation(self):
        loginPage = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        rownum = 1
        rows = sheet1.row_values(rownum)
        
        loginPage.enter_username_password('admin', '123')
        
        cmn.creat_company(rows[0], rows[1], rows[3], rows[5], rows[6], rows[7], rows[8], rows[9], rows[10], rows[11], rows[12], rows[13], rows[14], rows[15], rows[16], rows[17], rows[18])
        cmn.assert_company_info(rownum)
        
        rownum = 5
        rows = sheet1.row_values(rownum)
        cmn.modify_company(rows[0], rows[1], rows[3], rows[5], rows[6], rows[7], rows[8], rows[9], rows[10], rows[11], rows[12], rows[13], rows[14], rows[15], rows[16], rows[17], rows[18])
        cmn.assert_company_info(rownum)
        
        rownum = 1
        rows = sheet1.row_values(rownum)
        cmn.assert_company_version_info(rows[0], rows[1], rows[2], rows[4], rows[7], rows[8], rows[9], rows[10], rows[11], rows[12], rows[13], rows[14], rows[15], rows[16], rows[17], rows[18])
        
        cmn.delete_company_after_test()
             
    def testCompanyVersionRecovery(self):
        loginPage = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        rownum = 5
        rows = sheet1.row_values(rownum)
        
        loginPage.enter_username_password('admin', '123')
        
        cmn.creat_company(rows[0], rows[1], rows[3], rows[5], rows[6], rows[7], rows[8], rows[9], rows[10], rows[11], rows[12], rows[13], rows[14], rows[15], rows[16], rows[17], rows[18])
        cmn.assert_company_info(rownum)
        
        rownum = 6
        rows = sheet1.row_values(rownum)
        cmn.modify_company(rows[0], rows[1], rows[3], rows[5], rows[6], rows[7], rows[8], rows[9], rows[10], rows[11], rows[12], rows[13], rows[14], rows[15], rows[16], rows[17], rows[18])
        cmn.assert_company_info(rownum)
        
        rownum = 5
        rows = sheet1.row_values(rownum)
        cmn.company_version_recovery()
        cmn.assert_company_info(5)
        
        cmn.delete_company_after_test()

    def testMaxSymbolInCompanyCreationForm(self): #max valid symbols count
        loginPage = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        
        loginPage.enter_username_password('admin', '123')
        
        print '\nFor 255, 50, 255: \n _______________'
        cmn.fill_company_creation_form('N'*255, 'S'*50, 'S'*248)
        cmn.assert_overlong_symbols_error_present() 
             
    def testMaxPlus1SymbolsInCompanyCreationForm(self): #max+1 symbols count
        loginPage = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        
        loginPage.enter_username_password('admin', '123')
        
        print '\nFor 256, 51, 256: \n _______________'
        cmn.fill_company_creation_form('N'*256, 'S'*51, 'S'*256)
        cmn.assert_overlong_symbols_error_present()
            
    def testMaxMinus1SymbolsInCompanyCreationForm(self): #max-1 symbols count
        loginPage = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        
        loginPage.enter_username_password('admin', '123')
        
        print '\nFor 254, 49, 254: \n _______________'
        cmn.fill_company_creation_form('N'*254, 'S'*49, 'S'*247)
        cmn.assert_overlong_symbols_error_present()
    
    '''def testAssertTrashCompanyInfo(self):
        loginPage = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        rownum = 2
        rows = sheet1.row_values(rownum)
        
        loginPage.enter_username_password('admin', '123')

        cmn.creat_company(rows[0], rows[1], rows[3], rows[5], rows[6], rows[7], rows[8], rows[9], rows[10], rows[11], rows[12], rows[13], rows[14], rows[15], rows[16], rows[17], rows[18])
        cmn.delete_company_from_companycard()
        cmn.assert_deleted_company(rows[0], rows[1], rows[2], rows[4], rows[7], rows[8], rows[9], rows[10])'''
        
    '''def testRecoverDeletedCompany(self):
        loginPage = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        rownum = 2
        rows = sheet1.row_values(rownum)
        
        loginPage.enter_username_password('admin', '123')
        
        cmn.creat_company(rows[0], rows[1], rows[3], rows[5], rows[6], rows[7], rows[8], rows[9], rows[10], rows[11], rows[12], rows[13], rows[14], rows[15], rows[16], rows[17], rows[18])
        cmn.delete_company_from_companycard()
        cmn.recover_deleted_company(rows[1])
        cmn.assert_company_info(rownum)'''
        
    def testCompanyCreationForDB(self):
        loginPage = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        
        loginPage.enter_username_password('admin', '123')
        
        for i in range(10):
            Name = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(8))
            ShortName = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(8))
            cmn.create_short_company(Name, ShortName)
            
    def testCompanyCreationForDemo(self):
        loginPage = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\Company_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        for i in range(13):
            rownum = (i+1)
            rows = sheet1.row_values(rownum)
            cmn.creat_company(rows[0], rows[1], rows[3], rows[5], rows[6], rows[7], rows[8], rows[9], rows[10], rows[11], rows[12], rows[13], rows[14], rows[15], rows[16], rows[17], rows[18])         

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()