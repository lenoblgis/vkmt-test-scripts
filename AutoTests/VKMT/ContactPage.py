# -*- coding: utf-8 -*-
'''
@author: smirnov
'''

from locators import ContactSectionLocators, TrashSectionLocators, CompanySectionLocators
import time
from selenium.webdriver.common.by import By

class ContactPage():
    
    def __init__(self, driver):
        self.driver = driver
        
    def creat_contact(self, SecondName, FirstName, MiddleName, Possition, PersonalPhone, OfficialPhone, Email, Address, *args, **kwargs):
        time.sleep(0.8)
        self.driver.find_element(*ContactSectionLocators.contactSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.creatContactButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.contactLastNameField).send_keys(SecondName)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactFirstNameField).send_keys(FirstName)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactMiddleNameField).send_keys(MiddleName)
        time.sleep(0.2)
        if 'CompanyShortName' in kwargs: 
            self.driver.find_element(*ContactSectionLocators.contactCompanySelector).click()
            time.sleep(0.5)
            self.driver.find_element_by_xpath("//*[contains(text(), '%s')]" % kwargs['CompanyShortName']).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.contactPositionField).send_keys(Possition)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactPersonalPhoneField).send_keys(PersonalPhone)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactOfficiallPhoneField).send_keys(OfficialPhone)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactEmailPhoneField).send_keys(Email)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactAdressField).send_keys(Address)
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.saveContactDataButton).click()
        time.sleep(1)
        
    def creat_contact_via_company(self, SecondName, FirstName, MiddleName, Possition, PersonalPhone, OfficialPhone, Email):
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.contactWindowInCompanyButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.createContactViaCompanyBurron).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.contactLastNameField).send_keys(SecondName)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactFirstNameField).send_keys(FirstName)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactMiddleNameField).send_keys(MiddleName)
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.contactPositionField).send_keys(Possition)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactPersonalPhoneField).send_keys(PersonalPhone)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactOfficiallPhoneField).send_keys(OfficialPhone)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactEmailPhoneField).send_keys(Email)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.saveContactDataButtonViaCompany).click()
        time.sleep(1)
        self.driver.find_element(*ContactSectionLocators.firstContactInCompany).click()
        time.sleep(0.5)
        
    def assert_contact_info(self, SecondName, FirstName, MiddleName, Position, PersonalPhone, OfficialPhone, Email, Address, *args, **kwargs):
        assert SecondName in self.driver.find_element(*ContactSectionLocators.contactInfo).text
        assert FirstName in self.driver.find_element(*ContactSectionLocators.contactInfo).text
        if MiddleName: assert MiddleName in self.driver.find_element(*ContactSectionLocators.contactInfo).text
        if 'CompanyName' in kwargs: assert kwargs['CompanyName'] in self.driver.find_element(*ContactSectionLocators.contactInfo).text
        if Position: assert Position in self.driver.find_element(*ContactSectionLocators.contactInfo).text
        if PersonalPhone: assert PersonalPhone in self.driver.find_element(*ContactSectionLocators.contactInfo).text
        if OfficialPhone: assert OfficialPhone in self.driver.find_element(*ContactSectionLocators.contactInfo).text
        if Email: assert Email in self.driver.find_element(*ContactSectionLocators.contactInfo).text
        if Address: assert Address in self.driver.find_element(*ContactSectionLocators.contactInfo).text
    
    def modify_contact(self, SecondName, FirstName, MiddleName, Position, PersonalPhone, OfficialPhone, Email, Address):
        self.driver.find_element(*ContactSectionLocators.contactModificationButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.contactLastNameField).clear()
        self.driver.find_element(*ContactSectionLocators.contactLastNameField).send_keys(SecondName)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactFirstNameField).clear()
        self.driver.find_element(*ContactSectionLocators.contactFirstNameField).send_keys(FirstName)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactMiddleNameField).clear()
        self.driver.find_element(*ContactSectionLocators.contactMiddleNameField).send_keys(MiddleName)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactPositionField).clear()
        self.driver.find_element(*ContactSectionLocators.contactPositionField).send_keys(Position)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactPersonalPhoneField).clear()
        self.driver.find_element(*ContactSectionLocators.contactPersonalPhoneField).send_keys(PersonalPhone)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactOfficiallPhoneField).clear()
        self.driver.find_element(*ContactSectionLocators.contactOfficiallPhoneField).send_keys(OfficialPhone)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactEmailPhoneField).clear()
        self.driver.find_element(*ContactSectionLocators.contactEmailPhoneField).send_keys(Email)
        time.sleep(0.2)
        self.driver.find_element(*ContactSectionLocators.contactAdressField).clear()
        self.driver.find_element(*ContactSectionLocators.contactAdressField).send_keys(Address)
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.saveContactDataButton).click()
        time.sleep(1)   
        
    def assert_contact_version_creation(self):
        assert u'2 (текущая)' in self.driver.find_element(*ContactSectionLocators.listOfVersion).text
        assert '1' in self.driver.find_element(*ContactSectionLocators.listOfVersion).text
        time.sleep(0.5)
        
    def assert_contact_version_info(self, LastName, FirstName, MiddleName, CompanyName, Position, PersonalPhone, OfficialPhone, Email, Address):
        self.driver.find_element(*ContactSectionLocators.versionSelector).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.previousVersionButton).click()
        time.sleep(0.5)
        assert LastName in self.driver.find_element(*ContactSectionLocators.contactInfo).text
        assert FirstName in self.driver.find_element(*ContactSectionLocators.contactInfo).text
        if MiddleName: assert MiddleName in self.driver.find_element(*ContactSectionLocators.contactInfo).text
        if CompanyName: assert CompanyName in self.driver.find_element(*ContactSectionLocators.contactInfo).text
        if Position: assert Position in self.driver.find_element(*ContactSectionLocators.contactInfo).text
        if PersonalPhone: assert PersonalPhone in self.driver.find_element(*ContactSectionLocators.contactInfo).text
        if OfficialPhone: assert OfficialPhone in self.driver.find_element(*ContactSectionLocators.contactInfo).text
        if Email: assert Email in self.driver.find_element(*ContactSectionLocators.contactInfo).text
        if Address: assert Address in self.driver.find_element(*ContactSectionLocators.contactInfo).text
           
    def contact_version_recovery(self):
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.versionSelector).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.previousVersionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.contactVersionRecoveryButton).click()
        time.sleep(0.5)
        
    def delete_contact(self, SecondName):
        self.driver.find_element(*ContactSectionLocators.contactSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.contextSearchField).send_keys(SecondName)
        time.sleep(1)
        self.driver.find_element(*ContactSectionLocators.firstStringInList).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.contactDeleteButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.deleteConfirmationButton).click()
        time.sleep(0.5)
        self.driver.find_element(*TrashSectionLocators.trashSectionButton).click()
        time.sleep(0.5)
        
    def assert_deleted_contact(self, LastName, FirstName, MiddleName, CompanyName, Possition, PersonalPhone, OfficialPhone, Email, Address):
        self.driver.find_element(*TrashSectionLocators.trashSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(By.PARTIAL_LINK_TEXT, u"Макотченко").click()
        time.sleep(0.5)
        if LastName: assert LastName in self.driver.find_element(*TrashSectionLocators.contactTrashInfo).text
        if FirstName: assert FirstName in self.driver.find_element(*TrashSectionLocators.contactTrashInfo).text
        if MiddleName: assert MiddleName in self.driver.find_element(*TrashSectionLocators.contactTrashInfo).text
        if CompanyName: assert CompanyName in self.driver.find_element(*TrashSectionLocators.contactTrashInfo).text 
        if Possition: assert Possition in self.driver.find_element(*TrashSectionLocators.contactTrashInfo).text 
        if PersonalPhone: assert PersonalPhone in self.driver.find_element(*TrashSectionLocators.contactTrashInfo).text 
        if OfficialPhone: assert OfficialPhone in self.driver.find_element(*TrashSectionLocators.contactTrashInfo).text 
        if Email: assert Email in self.driver.find_element(*TrashSectionLocators.contactTrashInfo).text 
        if Address: assert Address in self.driver.find_element(*TrashSectionLocators.contactTrashInfo).text 
        
    def search_contact_by_fields(self, FieldForSearch, FieldForAsserting):
        self.driver.find_element(*ContactSectionLocators.contactSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.contextSearchField).send_keys(FieldForSearch)
        time.sleep(2)
        assert FieldForAsserting in self.driver.find_element(*ContactSectionLocators.firstStringInList).text
        
    def assert_overlong_symbols_error_present(self):
        try:
            assert u'Слишком длинно.' in self.driver.find_element(*ContactSectionLocators.overLongLastNameError).text
            print 'Error for LastName symbols'
        except:
            print 'For LastName symbols is OK'
        
        try:
            assert u'Слишком длинно.' in self.driver.find_element(*ContactSectionLocators.overLongFirstNameError).text
            print 'Error for FirstName symbols'
        except:
            print 'For FirstName symbols is OK'
            
        try:
            assert u'Слишком длинно.' in self.driver.find_element(*ContactSectionLocators.overLongMiddleNameError).text
            print 'Error for MiddleName symbols'
        except:
            print 'For MiddleName symbols is OK'
            
        try:
            assert u'Слишком длинно.' in self.driver.find_element(*ContactSectionLocators.overLongPossitionError).text
            print 'Error for Possition symbols'
        except:
            print 'For Possition symbols is OK'
        
    def fill_contact_creation_form(self, LastName, FirstName, MiddleName, Possition, Address):
        self.driver.find_element(*ContactSectionLocators.contactSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.creatContactButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.contactLastNameField).send_keys(LastName)
        time.sleep(0.3)
        self.driver.find_element(*ContactSectionLocators.contactFirstNameField).send_keys(FirstName)
        time.sleep(0.3)
        self.driver.find_element(*ContactSectionLocators.contactMiddleNameField).send_keys(MiddleName)
        time.sleep(0.3)
        self.driver.find_element(*ContactSectionLocators.contactPositionField).send_keys(Possition)
        time.sleep(0.3)
        self.driver.find_element(*ContactSectionLocators.contactAdressField).send_keys(Address)
        
    def delete_contact_after_test(self):
        self.driver.find_element(*ContactSectionLocators.contactSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.firstStringInList).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.contactDeleteButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ContactSectionLocators.deleteConfirmationButton).click()
        time.sleep(0.5)
        
    def delete_company_after_test(self):
        self.driver.find_element(*CompanySectionLocators.companySectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.firstStringInList).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.companyDeleteButton).click()
        time.sleep(0.5)
        self.driver.find_element(*CompanySectionLocators.deleteConfirmationButton).click()
        time.sleep(0.5)
        

        
        
        