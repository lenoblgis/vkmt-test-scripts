# -*- coding: utf-8 -*-
'''
@author: smirnov
'''
import unittest
from locators import LoginPageLocators, AccountSectionLocators
import time, xlrd
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import element_to_be_clickable

class ReturnDefaultData(object):
    
    def __init__(self, driver):
        self.driver = driver
    
    def returnDefaultAdminPassword(self):
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.accountSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.accountSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.adminAccountButton).click()
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.openAccountCardButton).click()
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.accountAdvincedOption).click()
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.changePasswordButton).click()
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.newPasswordField).send_keys('123')
        self.driver.find_element(*AccountSectionLocators.repeatNewPasswordField).send_keys('123')
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.acceptNewPasswordButton).click()
        
class LoginPage(object):

    def __init__(self, driver):
        self.driver = driver
        
    def enter_username_password(self, username, password):
        self.driver.find_element(*LoginPageLocators.usernameField).send_keys(username)
        self.driver.find_element(*LoginPageLocators.paswordField).send_keys(password)
        #time.sleep(0.5)
        #WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//button[@type='button']"))).click()
        #WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//button[@type='button']"))).click()
        self.driver.find_element(*LoginPageLocators.loginButton).click()
        
    def click_login_button(self):
        self.driver.find_element(*LoginPageLocators.loginButton).click()
        
    def is_loginButton_clickable(self):
        assert self.driver.find_element(*LoginPageLocators.loginButton).is_displayed()
        
    def is_login_error_present(self):
        try:
            assert self.driver.find_element(*LoginPageLocators.loginError).text == 'Bad credentials'
        except:
            print 'No login error'
            
class AccountPage(object):
    
    def __init__(self, driver):
        self.driver = driver
    
    def enter_account_data(self, lastname, firstname, position):
        time.sleep(1)
        self.driver.find_element(*AccountSectionLocators.accountLastNameField).clear()
        self.driver.find_element(*AccountSectionLocators.accountFirstNameField).clear()
        self.driver.find_element(*AccountSectionLocators.accountPositionField).clear()
        self.driver.find_element(*AccountSectionLocators.accountLastNameField).send_keys(lastname)
        self.driver.find_element(*AccountSectionLocators.accountFirstNameField).send_keys(firstname)
        self.driver.find_element(*AccountSectionLocators.accountPositionField).send_keys(position)  
        
    def click_account_section_button(self):
        time.sleep(1)
        self.driver.find_element(*AccountSectionLocators.accountSectionButton).click()
        
    def click_admin_account(self):
        time.sleep(1)
        self.driver.find_element(*AccountSectionLocators.adminAccountButton).click()
        
    def click_modifying_button_on_quick_panel(self):
        time.sleep(1)
        self.driver.find_element(*AccountSectionLocators.quickAccountModifyingButton).click()
        
    def click_save_account_button(self):
        time.sleep(1)
        self.driver.find_element(*AccountSectionLocators.saveModifyingButton).click()
        time.sleep(0.5)
        
    def create_account(self, username, password, lastname, firstname, middlename):   
        self.driver.find_element(*AccountSectionLocators.createAccountButton).click()
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.accountUsernameField).send_keys(username)
        self.driver.find_element(*AccountSectionLocators.accountPasswordField).send_keys(password)
        self.driver.find_element(*AccountSectionLocators.accountLastNameField).send_keys(lastname)
        self.driver.find_element(*AccountSectionLocators.accountFirstNameField).send_keys(firstname)
        self.driver.find_element(*AccountSectionLocators.accountMiddleNameField).send_keys(middlename)
        AccountPage.click_save_account_button(self)
        
    def modify_account(self, email, phone, position):
        time.sleep(1)
        self.driver.find_element(*AccountSectionLocators.accountModifyingButton).click()
        time.sleep(1)
        self.driver.find_element(*AccountSectionLocators.accountEmailField).send_keys(email)
        self.driver.find_element(*AccountSectionLocators.accountPhoneField).send_keys(phone)
        self.driver.find_element(*AccountSectionLocators.accountPositionField).send_keys(position)
        AccountPage.click_save_account_button(self)
        time.sleep(0.5)
        
    def assert_change_admin_password(self, password, password2):
        AccountPage.click_account_section_button(self)
        AccountPage.click_admin_account(self)
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.openAccountCardButton).click()
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.accountAdvincedOption).click()
        time.sleep(1)
        self.driver.find_element(*AccountSectionLocators.changePasswordButton).click()
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.newPasswordField).send_keys(password)
        self.driver.find_element(*AccountSectionLocators.repeatNewPasswordField).send_keys(password2)
        try:
            self.driver.find_element(*AccountSectionLocators.acceptNewPasswordButton).click()
        except:
            print 'Can not change the password.'      
        
    def assert_account_info(self, rownum):
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\Accounts_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        rows = sheet1.row_values(rownum)
        assert rows[0] in self.driver.find_element(*AccountSectionLocators.accountInfo).text
        assert rows[2] in self.driver.find_element(*AccountSectionLocators.accountInfo).text
        assert rows[3] in self.driver.find_element(*AccountSectionLocators.accountInfo).text
        assert rows[4] in self.driver.find_element(*AccountSectionLocators.accountInfo).text
        assert rows[5] in self.driver.find_element(*AccountSectionLocators.accountInfo).text
        if rows[6]: assert rows[6] in self.driver.find_element(*AccountSectionLocators.accountInfo).text
            
    def empty_fields_assert(self):
        self.driver.find_element(*AccountSectionLocators.accountSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.createAccountButton).click()
        time.sleep(1)
        try:
            AccountPage.click_save_account_button(self)
        except:
            print 'Can not login. All of the fields are empty'
    
        self.driver.find_element(*AccountSectionLocators.accountUsernameField).send_keys('TestName12')
        try:
            AccountPage.click_save_account_button(self)
        except:
            print 'Can not login. Password, FirstName, Lastname fields are empty'
            
        self.driver.find_element(*AccountSectionLocators.accountPasswordField).send_keys('123')
        try:
            AccountPage.click_save_account_button(self)
        except:
            print 'Can not login. FirstName, Lastname fields are empty'
            
        self.driver.find_element(*AccountSectionLocators.accountLastNameField).send_keys('Testlastname')
        try:
            AccountPage.click_save_account_button(self)
        except:
            print 'Can not login. FirstName field is empty'
            
        self.driver.find_element(*AccountSectionLocators.accountFirstNameField).send_keys('Testfirstname')
        try:
            AccountPage.click_save_account_button(self)
        except:
            print 'Can not login'
        time.sleep(1)
    
    def large_symbols_count_assert(self):
        self.driver.find_element(*AccountSectionLocators.accountSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.createAccountButton).click()
        time.sleep(1)
        self.driver.find_element(*AccountSectionLocators.accountUsernameField).send_keys('p' * 33)
        self.driver.find_element(*AccountSectionLocators.accountPasswordField).send_keys('e' * 32)
        self.driver.find_element(*AccountSectionLocators.accountLastNameField).send_keys('s' * 50)
        self.driver.find_element(*AccountSectionLocators.accountFirstNameField).send_keys('t' * 50)
        self.driver.find_element(*AccountSectionLocators.accountMiddleNameField).send_keys('s' * 50)
        
        try:
            AccountPage.click_save_account_button(self)
        except:
            print 'Can not login. Large symbols count'
           
        self.driver.find_element(*AccountSectionLocators.accountUsernameField).clear()
        self.driver.find_element(*AccountSectionLocators.accountUsernameField).send_keys('o' * 32)
        self.driver.find_element(*AccountSectionLocators.accountPasswordField).clear()
        self.driver.find_element(*AccountSectionLocators.accountPasswordField).send_keys('e' * 33)
        try:
            AccountPage.click_save_account_button(self)
        except:
            print 'Can not login. Large symbols count'
            
        self.driver.find_element(*AccountSectionLocators.accountPasswordField).clear()
        self.driver.find_element(*AccountSectionLocators.accountPasswordField).send_keys('e' * 32)
        self.driver.find_element(*AccountSectionLocators.accountLastNameField).clear()
        self.driver.find_element(*AccountSectionLocators.accountLastNameField).send_keys('s' * 51)
        try:
            AccountPage.click_save_account_button(self)
        except:
            print 'Can not login. Large symbols count'
            
        self.driver.find_element(*AccountSectionLocators.accountLastNameField).clear()
        self.driver.find_element(*AccountSectionLocators.accountLastNameField).send_keys('s' * 50)
        self.driver.find_element(*AccountSectionLocators.accountFirstNameField).clear()
        self.driver.find_element(*AccountSectionLocators.accountFirstNameField).send_keys('t' * 51)
        try:
            AccountPage.click_save_account_button(self)
        except:
            print 'Can not login. Large symbols count'
            
        self.driver.find_element(*AccountSectionLocators.accountFirstNameField).clear()
        self.driver.find_element(*AccountSectionLocators.accountFirstNameField).send_keys('t' * 50)
        self.driver.find_element(*AccountSectionLocators.accountMiddleNameField).clear()
        self.driver.find_element(*AccountSectionLocators.accountMiddleNameField).send_keys('s' * 51)
        try:
            AccountPage.click_save_account_button(self)
        except:
            print 'Can not login. Large symbols count'
            
        self.driver.find_element(*AccountSectionLocators.accountMiddleNameField).clear()
        self.driver.find_element(*AccountSectionLocators.accountMiddleNameField).send_keys('s' * 50)
        try:
            AccountPage.click_save_account_button(self)
        except:
            print 'Can not login. Large symbols count'
            
    def change_admin_data(self, LastName, FirstName):
        time.sleep(1)
        self.driver.find_element(*AccountSectionLocators.accountSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.adminAccountButton).click()
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.quickAccountModifyingButton).click()
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.accountLastNameField).clear()
        self.driver.find_element(*AccountSectionLocators.accountFirstNameField).clear()
        self.driver.find_element(*AccountSectionLocators.accountLastNameField).send_keys(LastName)
        self.driver.find_element(*AccountSectionLocators.accountFirstNameField).send_keys(FirstName)
        time.sleep(0.5)
        self.driver.find_element(*AccountSectionLocators.saveModifyingButton).click()
        time.sleep(0.5)
            

if __name__ == "__main__":
    unittest.main()   