# -*- coding: utf-8 -*-
'''
@author: smirnov
'''

import unittest
from selenium import webdriver
from locators import DashboardLocators
from AccountPage import LoginPage
from ContactPage import ContactPage
from CompanyPage import CompanySection
from AccountPage import AccountPage
from AccountTest import TestAccountForms
import time
from ActionPage import ActionPage
from ProjectPage import ProjectPage

class TestDashboardSection(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://vkmt-test.lenoblgis.ru/#/login')
        self.driver.implicitly_wait(5)
        self.driver.maximize_window() # max browser window size
        
    def tearDown(self):
        self.driver.close()
        
    def testCompanyCreationNote(self):
        lgn = LoginPage(self.driver)
        acc = AccountPage(self.driver)
        cmn = CompanySection(self.driver)
        
        lgn.enter_username_password('admin', '123')
        
        acc.change_admin_data('LastName', 'FirstName')
        cmn.create_short_company('Name', 'ShortName')
        
        time.sleep(0.5)
        self.driver.find_element(*DashboardLocators.dashboardSectionButton).click()
        time.sleep(2)
        assert u'LastName FirstName' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'создание' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'ShortName (Государственные ЛПУ)' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        
    def testCompanyModificationNote(self):
        lgn = LoginPage(self.driver)
        acc = AccountPage(self.driver)
        cmn = CompanySection(self.driver)
        
        lgn.enter_username_password('admin', '123')
        
        acc.change_admin_data('LastName', 'FirstName')
        cmn.create_short_company('Name', 'ShortName')
        cmn.modify_short_company('Nameless', 'ShortlessName')
        
        self.driver.find_element(*DashboardLocators.dashboardSectionButton).click()
        time.sleep(2)
        assert u'LastName FirstName' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'редактирование' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'ShortlessName (Государственные ЛПУ)' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        
    def testCompanyDeletingNote(self):
        lgn = LoginPage(self.driver)
        acc = AccountPage(self.driver)
        cmn = CompanySection(self.driver)
        
        lgn.enter_username_password('admin', '123')
        
        acc.change_admin_data('LastName', 'FirstName')
        cmn.create_short_company('Name', 'ShortName')
        cmn.delete_company_from_companycard()
        
        self.driver.find_element(*DashboardLocators.dashboardSectionButton).click()
        time.sleep(2)
        assert u'LastName FirstName' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'удаление' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'ShortName (Государственные ЛПУ)' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        
        
    def testContactCreationNote(self):
        lgn = LoginPage(self.driver)
        acc = AccountPage(self.driver)
        cont = ContactPage(self.driver)
        
        lgn.enter_username_password('admin', '123')
        
        acc.change_admin_data('LastName', 'FirstName')
        cont.creat_contact('SecondName', 'FirstName', 'MiddleName', 'Possition', '11111111111', '11111111111', 'Email@mail.ru', 'Address')
        
        self.driver.find_element(*DashboardLocators.dashboardSectionButton).click()
        time.sleep(2)
        assert u'LastName FirstName' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'создание' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'SecondName FirstName MiddleName (Possition)' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        
    def testContactModificationNote(self):
        lgn = LoginPage(self.driver)
        acc = AccountPage(self.driver)
        cont = ContactPage(self.driver)
        
        lgn.enter_username_password('admin', '123')
        acc.change_admin_data('LastName', 'FirstName')
        
        cont.creat_contact('SecondName', 'FirstName', 'MiddleName', 'Possition', '11111111111', '11111111111', 'Email@mail.ru', 'Address')
        cont.modify_contact('SecondName', 'FirstName', 'MiddleName', 'Possition', '11111111111', '11111111111', 'Email@mail.ru', 'Address')   
             
        self.driver.find_element(*DashboardLocators.dashboardSectionButton).click()
        time.sleep(2)
        assert u'LastName FirstName' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'редактирование' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'SecondName FirstName MiddleName (Possition)' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        
    def testContactDeletingNote(self):
        lgn = LoginPage(self.driver)
        acc = AccountPage(self.driver)
        cont = ContactPage(self.driver)
        
        lgn.enter_username_password('admin', '123')
        acc.change_admin_data('LastName', 'FirstName')
        
        cont.creat_contact('SecondName', 'FirstName', 'MiddleName', 'Possition', '11111111111', '11111111111', 'Email@mail.ru', 'Address')
        cont.delete_contact_after_test() 
             
        self.driver.find_element(*DashboardLocators.dashboardSectionButton).click()
        time.sleep(2)
        assert u'LastName FirstName' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'удаление' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'SecondName FirstName MiddleName (Possition)' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        
    def testActionCreationNote(self):
        lgn = LoginPage(self.driver)
        acc = AccountPage(self.driver)
        cmn = CompanySection(self.driver)
        act = ActionPage(self.driver)
        
        lgn.enter_username_password('admin', '123')
        acc.change_admin_data('LastName', 'FirstName')
        
        cmn.create_short_company('Name', 'ShortName')
        act.create_action(1, 1, 'ShortName', 1, 'Object')
        
        self.driver.find_element(*DashboardLocators.dashboardSectionButton).click()
        time.sleep(2)
        assert u'LastName FirstName' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'создание' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'Встреча : Object' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        
    def testActionModificationNote(self):
        lgn = LoginPage(self.driver)
        acc = AccountPage(self.driver)
        cmn = CompanySection(self.driver)
        act = ActionPage(self.driver)
        
        lgn.enter_username_password('admin', '123')
        acc.change_admin_data('LastName', 'FirstName')
        
        cmn.create_short_company('Name', 'ShortName')
        act.create_action(1, 1, 'ShortName', 1, 'Object')
        act.modify_action(1, 1, 'ShortlessName', 1, 'Objectless')      
        
        self.driver.find_element(*DashboardLocators.dashboardSectionButton).click()
        time.sleep(2)
        assert u'LastName FirstName' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'редактирование' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'Встреча : Object' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        
    def testActionDeletingNote(self):
        lgn = LoginPage(self.driver)
        acc = AccountPage(self.driver)
        cmn = CompanySection(self.driver)
        act = ActionPage(self.driver)
        
        lgn.enter_username_password('admin', '123')
        acc.change_admin_data('LastName', 'FirstName')
        
        cmn.create_short_company('Name', 'ShortName')
        act.create_action(1, 1, 'ShortName', 1, 'Object')
        act.delete_action()
        
        self.driver.find_element(*DashboardLocators.dashboardSectionButton).click()
        time.sleep(2)
        #assert u'LastName FirstName' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        #assert u'редактирование' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        #assert u'Встреча : Object' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'LastName FirstName удаление playlist_add_check Встреча : Object' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        
    def testProjectCreationNote(self):
        lgn = LoginPage(self.driver)
        acc = AccountPage(self.driver)
        cmn = CompanySection(self.driver)
        prj = ProjectPage(self.driver)
        
        lgn.enter_username_password('admin', '123')
        acc.change_admin_data('LastName', 'FirstName')
        
        cmn.create_short_company('Name', 'ShortName')
        prj.create_project('ProjectName', 'ShortName', 'ShortName', 1, 1, 'RealizationDate', 1, 'EstimateBudget', 'ProjectContext')
        
        self.driver.find_element(*DashboardLocators.dashboardSectionButton).click()
        time.sleep(2)
        #assert u'LastName FirstName' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        #assert u'создание' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        #assert u'ProjectName(ShortName)' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'LastName FirstName создание inbox ProjectName(ShortName)' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        
    def testProjectModificationNote(self):
        lgn = LoginPage(self.driver)
        acc = AccountPage(self.driver)
        cmn = CompanySection(self.driver)
        prj = ProjectPage(self.driver)
        
        lgn.enter_username_password('admin', '123')
        acc.change_admin_data('LastName', 'FirstName')
        
        cmn.create_short_company('Name', 'ShortName')
        prj.create_project('ProjectName', 'ShortName', 'ShortName', 1, 1, 'RealizationDate', 1, 'EstimateBudget', 'ProjectContext')
        prj.modify_project('ProjectName', 'ShortName', 'ShortName', 1, 1, 'RealizationDate', 1, 'EstimateBudget', 'ProjectContext')
        
        self.driver.find_element(*DashboardLocators.dashboardSectionButton).click()
        time.sleep(2)
        #assert u'LastName FirstName' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        #assert u'создание' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        #assert u'ProjectName(ShortName)' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        assert u'LastName FirstName редактирование inbox ProjectName(ShortName)' in self.driver.find_element(*DashboardLocators.firstAuditArea).text
        
    def testProjectDeletingNote(self):
        lgn = LoginPage(self.driver)
        acc = AccountPage(self.driver)
        cmn = CompanySection(self.driver)
        prj = ProjectPage(self.driver)
        
        lgn.enter_username_password('admin', '123')
        acc.change_admin_data('LastName', 'FirstName')
        
        cmn.create_short_company('Name', 'ShortName')
        prj.create_project('ProjectName', 'ShortName', 'ShortName', 1, 1, 'RealizationDate', 1, 'EstimateBudget', 'ProjectContext')
        
        prj.delete_project()

        self.driver.find_element(*DashboardLocators.dashboardSectionButton).click()
        time.sleep(2)
        
        assert u'LastName FirstName удаление inbox ProjectName(ShortName)' in self.driver.find_element(*DashboardLocators.firstAuditArea).text   
        
        
        
        
        
        