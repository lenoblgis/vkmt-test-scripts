# -*- coding: utf-8 -*-
'''
@author: smirnov
'''

from locators import AccountSectionLocators, TripSectionLocators
import time
from selenium.webdriver.common.keys import Keys

class TripPage():
    
    def __init__(self, driver):
        self.driver = driver
        
    def create_trip(self, EmployeeLastName, TripAddress, PlannedPeriod, HotelCost, OverheadCost, PresentationCost):
        time.sleep(1)
        self.driver.find_element(*TripSectionLocators.tripSectionSelector).click()
        time.sleep(0.5)
        self.driver.find_element(*TripSectionLocators.createTripButton).click()
        time.sleep(0.5)
        self.driver.find_element(*TripSectionLocators.tripEmloyeeSelector).click()
        time.sleep(0.5)
        self.driver.find_element(*TripSectionLocators.tripEmployeeSearchField).send_keys(EmployeeLastName)
        time.sleep(0.5)
        self.driver.find_element(*TripSectionLocators.firstStringInEmployeeSearchResult).click()
        time.sleep(0.5)
        self.driver.find_element(*TripSectionLocators.tripAddressField).send_keys(TripAddress)
        time.sleep(2)
        self.driver.find_element(*TripSectionLocators.tripAddressField).send_keys(Keys.ARROW_DOWN)
        self.driver.find_element(*TripSectionLocators.tripAddressField).send_keys(Keys.ENTER)
        time.sleep(0.5)
        self.driver.find_element(*TripSectionLocators.startPeriodCalendarButton).click()
        self.driver.find_element(*TripSectionLocators.startPeriodCalendarField).send_keys(Keys.ENTER)
        time.sleep(0.5)
        self.driver.find_element(*TripSectionLocators.endPeriodCalendarButton).click()
        self.driver.find_element(*TripSectionLocators.endPeriodCalendarField).send_keys(Keys.ENTER)
        time.sleep(0.5)
        self.driver.find_element(*TripSectionLocators.tripPlannedPeriodField).send_keys(PlannedPeriod)
        time.sleep(0.5)
        self.driver.find_element(*TripSectionLocators.hotelCostField).send_keys(HotelCost)
        time.sleep(0.5)
        self.driver.find_element(*TripSectionLocators.overheadCost).send_keys(OverheadCost)
        time.sleep(0.5)
        self.driver.find_element(*TripSectionLocators.presentationCost).send_keys(PresentationCost)
        time.sleep(0.5)
        self.driver.find_element(*TripSectionLocators.saveTripButton).click()
        time.sleep(3)
        
        