# -*- coding: utf-8 -*-
'''
@author: smirnov
'''
import unittest
from selenium import webdriver
from AccountPage import LoginPage
from PricePage import PricePage
from EquipmentPage import EquipmentPage
import xlrd
import random


class TestPriceSection(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://vkmt-test.lenoblgis.ru/#/login')
        self.driver.implicitly_wait(5)
        self.driver.maximize_window() # max browser window size
        
    def tearDown(self):
        self.driver.close()

    def testPriceCreation(self):
        loginPage = LoginPage(self.driver)
        prc = PricePage(self.driver)
        equipmentPage = EquipmentPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Eqiupment_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 1
        rows = sheet1.row_values(rownum)
        equipmentPage.creat_equipment(rows[2], rows[4], rows[0], rows[1])
        prc.creat_price(rows[2], rows[8])
        prc.assert_price_info_in_list(rows[2], rows[4], rows[13])
        prc.set_25_in_page()
        prc.choice_current_price(rows[2])
        prc.assert_price_info_in_card(rows[2], rows[4], rows[13]) #аргументы - стобцы, по которым проходит проверка
        
        prc.delete_price_after_test()
        equipmentPage.delete_equipment_after_test()
            
    def testPriceModification(self):
        loginPage = LoginPage(self.driver)
        prc = PricePage(self.driver)
        equipmentPage = EquipmentPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Eqiupment_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 2
        rows = sheet1.row_values(rownum)
        equipmentPage.creat_equipment(rows[2], rows[4], rows[0], rows[1])
        prc.creat_price(rows[2], rows[8])
        
        rownum = 3
        rows = sheet1.row_values(rownum)
        equipmentPage.creat_equipment(rows[2], rows[4], rows[0], rows[1])
        prc.modify_price(rows[2], rows[8])
        prc.assert_price_info_in_list(rows[2], rows[8], rows[14]) #аргументы - стобцы, по которым проходит проверка
        
        prc.delete_price_after_test()
        equipmentPage.delete_equipment_after_test()
        equipmentPage.delete_equipment_after_test()
        
    def testPriceDeleting(self):
        loginPage = LoginPage(self.driver)
        prc = PricePage(self.driver)
        equipmentPage = EquipmentPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Eqiupment_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 4
        rows = sheet1.row_values(rownum)
        equipmentPage.creat_equipment(rows[2], rows[4], rows[0], rows[1])
        prc.creat_price(rows[2], rows[8])
        prc.delete_price()
        prc.assert_if_price_deleted(rows[2])
        
        equipmentPage.delete_equipment_after_test()
        
    def testContextPricetSearchByModel(self):
        loginPage = LoginPage(self.driver)
        prc = PricePage(self.driver)
        equipmentPage = EquipmentPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Eqiupment_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 4
        rows = sheet1.row_values(rownum)
        equipmentPage.creat_equipment(rows[2], rows[4], rows[0], rows[1])
        prc.creat_price(rows[2], rows[8])
        prc.search_price_by_field(rows[2])
        
        prc.delete_price_after_test()
        equipmentPage.delete_equipment_after_test()
        
    def testCreatePriceForDB(self):
        loginPage = LoginPage(self.driver)
        prc = PricePage(self.driver)
        equipmentPage = EquipmentPage(self.driver)
        
        loginPage.enter_username_password('admin', '123')
        
        for i in range(80):
            model = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(10))
            manufacturer = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(10))
            types = 1
            kinds = random.randint(1, 12)
            price = '10000'
            equipmentPage.creat_equipment(model, manufacturer, kinds, types)
            prc.creat_price(model, price)
            
    def testPriceCreationForDemo(self):
        loginPage = LoginPage(self.driver)
        prc = PricePage(self.driver)
        equipmentPage = EquipmentPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Eqiupment_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        for i in range(12):
            rownum = 1
            rows = sheet1.row_values(rownum)
            equipmentPage.creat_equipment(rows[2], rows[4], rows[0], rows[1])
            prc.creat_price(rows[2], rows[8])
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()