# -*- coding: utf-8 -*-
'''
@author: smirnov
'''

from locators import PriceSectionLocators
import time
from selenium.webdriver.common.keys import Keys
from datetime import datetime

class PricePage(object):

    def __init__(self, driver):
        self.driver = driver
        
    def creat_price(self, EquipmentModel, Price):
        time.sleep(0.8)
        self.driver.find_element(*PriceSectionLocators.priceSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.creatPriceButton).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.equipmentField).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.equipmentSearchField).send_keys(EquipmentModel)
        time.sleep(1)
        self.driver.find_element(*PriceSectionLocators.firstEquipment).click()
        time.sleep(1)
        self.driver.find_element(*PriceSectionLocators.startPeriodButton).click()
        time.sleep(0.5)
        self.driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container/md-datepicker/div[1]/input").send_keys(Keys.ENTER)
        time.sleep(0.3)
        self.driver.find_element(*PriceSectionLocators.endPeriodButton).click()
        time.sleep(0.5)
        self.driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container/md-datepicker/div[1]/input").send_keys(Keys.ENTER)
        time.sleep(0.3)
        self.driver.find_element(*PriceSectionLocators.priceField).send_keys(Price)
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.savePriceButton).click()
        time.sleep(1)
        
    def create_random_price(self, Price):
        time.sleep(0.8)
        self.driver.find_element(*PriceSectionLocators.priceSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.creatPriceButton).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.equipmentField).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.firstEquipment).click()
        time.sleep(1)
        self.driver.find_element(*PriceSectionLocators.startPeriodButton).click()
        time.sleep(0.5)
        self.driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container/md-datepicker/div[1]/input").send_keys(Keys.ENTER)
        time.sleep(0.3)
        self.driver.find_element(*PriceSectionLocators.endPeriodButton).click()
        time.sleep(0.5)
        self.driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container/md-datepicker/div[1]/input").send_keys(Keys.ENTER)
        time.sleep(0.3)
        self.driver.find_element(*PriceSectionLocators.priceField).send_keys(Price)
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.savePriceButton).click()
        time.sleep(1)
        
        
    def choice_current_price(self, rows):
        self.driver.find_element_by_xpath("//*[contains(text(), '%s')]" % rows).click()
        time.sleep(0.3)
            
        
    def modify_price(self, EquipmentModel, Price):
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.priceSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.firstPriceInList).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.modifyingEquipmentButton).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.equipmentField).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.equipmentSearchField).send_keys(EquipmentModel)
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.firstEquipment).click()
        time.sleep(0.3)
        self.driver.find_element(*PriceSectionLocators.startPeriodButton).click()
        time.sleep(0.5)
        self.driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container/md-datepicker/div[1]/input").send_keys(Keys.ENTER)
        time.sleep(0.3)
        self.driver.find_element(*PriceSectionLocators.endPeriodButton).click()
        time.sleep(0.5)
        self.driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container/md-datepicker/div[1]/input").send_keys(Keys.ENTER)
        time.sleep(0.3)
        self.driver.find_element(*PriceSectionLocators.priceField).clear()
        self.driver.find_element(*PriceSectionLocators.priceField).send_keys(Price)
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.savePriceButton).click()
        time.sleep(1)
        
    def assert_price_info_in_card(self, EquipmentModel, Price): 
        self.driver.find_element(*PriceSectionLocators.priceSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.firstPriceInList).click()
        time.sleep(0.5)
        assert EquipmentModel in self.driver.find_element(*PriceSectionLocators.priceInfoInCard).text
        assert Price in self.driver.find_element(*PriceSectionLocators.priceInfoInCard).text
        assert datetime.now().strftime('%d.%m.%Y') in self.driver.find_element(*PriceSectionLocators.priceInfoInCard).text
        
    def assert_price_info_in_list(self, EquipmentModel, Price, CurrencySymbol): 
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.priceSectionButton).click()
        time.sleep(0.5)
        assert EquipmentModel in self.driver.find_element(*PriceSectionLocators.priceList).text
        assert Price in self.driver.find_element(*PriceSectionLocators.priceList).text
        assert datetime.now().strftime('%d.%m.%Y') in self.driver.find_element(*PriceSectionLocators.priceList).text
        assert CurrencySymbol in self.driver.find_element(*PriceSectionLocators.priceList).text
        
    def set_25_in_page(self):
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.countInList).click()
        time.sleep(0.3)
        self.driver.find_element(*PriceSectionLocators.InList25).click()    
        
    def delete_price_after_test(self):
        self.driver.find_element(*PriceSectionLocators.priceSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.firstPriceInList).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.deletePriceButton).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.deleteConfirmationButton).click()
        
    def delete_price(self):
        self.driver.find_element(*PriceSectionLocators.priceSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.firstPriceInList).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.deletePriceButton).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.deleteConfirmationButton).click()
        
    def assert_if_price_deleted(self, EquipmentModel):
        time.sleep(0.5)
        try: assert EquipmentModel in self.driver.find_element(*PriceSectionLocators.firstPriceInList).text
        except: print 'For testPriceDeleting: price has been removed'
        
    def search_price_by_field(self, FieldForSearch):
        self.driver.find_element(*PriceSectionLocators.priceSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*PriceSectionLocators.contextSearchField).send_keys(FieldForSearch)
        time.sleep(0.5)
        assert FieldForSearch in self.driver.find_element(*PriceSectionLocators.firstPriceInList).text
        
        
        
        
        