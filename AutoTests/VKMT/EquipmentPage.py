# -*- coding: utf-8 -*-
'''
@author: smirnov
'''

from locators import EquipmentSectionLocators
import time
import xlrd

class EquipmentPage(object):

    def __init__(self, driver):
        self.driver = driver
        
    def creat_equipment(self, model, manufacturer, kinds, types):
        time.sleep(1.3)
        self.driver.find_element(*EquipmentSectionLocators.equipmentSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*EquipmentSectionLocators.creatEquipmentButton).click()
        time.sleep(0.5)
        self.driver.find_element(*EquipmentSectionLocators.equipmentModelField).send_keys(model)
        self.driver.find_element(*EquipmentSectionLocators.equipmentManufacturerField).send_keys(manufacturer)
        time.sleep(0.5)
        EquipmentPage.kind_and_type_choice(self, kinds, types)
        self.driver.find_element(*EquipmentSectionLocators.saveEquipmentButton).click()
        time.sleep(0.5)
        
    def kind_and_type_choice(self, kinds, types):
        self.driver.find_element(*EquipmentSectionLocators.equipmentKindField).click()
        time.sleep(0.5)
        self.driver.find_element_by_xpath('/html/body/div[4]/md-select-menu/md-content/md-option[%s]/div' % kinds).click()
        time.sleep(0.5)
        self.driver.find_element(*EquipmentSectionLocators.equipmentTypeField).click()
        time.sleep(0.5)
        self.driver.find_element_by_xpath('/html/body/div[5]/md-select-menu/md-content/md-option[%s]/div' % types).click()
        time.sleep(0.5)
        
    def open_current_equipment_details(self, rows):
        time.sleep(1)
        self.driver.find_element_by_xpath("//*[contains(text(), '%s')]" % rows).click()
        time.sleep(0.5)
        
    def assert_equipment_info_in_card(self, rownum):
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\Eqiupment_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        rows = sheet1.row_values(rownum)
    
        assert rows[2] in self.driver.find_element(*EquipmentSectionLocators.equipmentInfoInCard).text
        assert rows[4] in self.driver.find_element(*EquipmentSectionLocators.equipmentInfoInCard).text
        
    def assert_equipment_info_in_list(self, rownum):
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\Eqiupment_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        rows = sheet1.row_values(rownum)
        
        self.driver.find_element(*EquipmentSectionLocators.equipmentSectionButton).click()
        assert rows[2] in self.driver.find_element(*EquipmentSectionLocators.equipmentInfoInList).text
        assert rows[4] in self.driver.find_element(*EquipmentSectionLocators.equipmentInfoInList).text
        
    def set_25_in_page(self):
        time.sleep(1)
        self.driver.find_element(*EquipmentSectionLocators.countInListButton).click()
        time.sleep(0.3)
        self.driver.find_element(*EquipmentSectionLocators.inList25Button).click()
        
    def delete_equipment_after_test(self):
        time.sleep(0.5)
        self.driver.find_element(*EquipmentSectionLocators.equipmentSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*EquipmentSectionLocators.firstStringInList).click()
        time.sleep(0.5)
        self.driver.find_element(*EquipmentSectionLocators.equipmentDeleteButton).click()
        time.sleep(0.5)
        self.driver.find_element(*EquipmentSectionLocators.deleteConfirmationButton).click()
        time.sleep(0.5)
        
    def modify_equipment(self, model, manufacturer, kinds, types):
        time.sleep(0.5)
        self.driver.find_element(*EquipmentSectionLocators.equipmentSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*EquipmentSectionLocators.firstStringInList).click()
        time.sleep(0.5)
        self.driver.find_element(*EquipmentSectionLocators.equipmentModificationButton).click()
        time.sleep(0.5)
        self.driver.find_element(*EquipmentSectionLocators.equipmentModelField).clear()
        self.driver.find_element(*EquipmentSectionLocators.equipmentModelField).send_keys(model)
        self.driver.find_element(*EquipmentSectionLocators.equipmentManufacturerField).clear()
        self.driver.find_element(*EquipmentSectionLocators.equipmentManufacturerField).send_keys(manufacturer)
        EquipmentPage.kind_and_type_choice(self, kinds, types)
        self.driver.find_element(*EquipmentSectionLocators.saveEquipmentButton).click()
        time.sleep(0.5)
        
    def assert_if_equipment_delete(self, model):
        time.sleep(0.5)
        self.driver.find_element(*EquipmentSectionLocators.equipmentSectionButton).click()
        time.sleep(0.5)
        try: assert model in self.driver.find_element(*EquipmentSectionLocators.firstStringInList).text
        except: print 'For testEquipmentDeleting: equipment has been removed'
        time.sleep(0.5)
        
    def search_equipment_by_field(self, FieldForSearch):
        time.sleep(0.5)
        self.driver.find_element(*EquipmentSectionLocators.equipmentSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*EquipmentSectionLocators.contextSearchField).send_keys(FieldForSearch)
        time.sleep(0.5)
        assert FieldForSearch in self.driver.find_element(*EquipmentSectionLocators.firstStringInList).text
        
    def fill_equipment_creation_form(self, Model, Manufacturer):
        time.sleep(0.5)
        self.driver.find_element(*EquipmentSectionLocators.equipmentSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*EquipmentSectionLocators.creatEquipmentButton).click()
        time.sleep(0.5)
        self.driver.find_element(*EquipmentSectionLocators.equipmentModelField).send_keys(Model)
        self.driver.find_element(*EquipmentSectionLocators.equipmentManufacturerField).send_keys(Manufacturer)
        time.sleep(0.3)
        self.driver.find_element(*EquipmentSectionLocators.equipmentModelField).click()
        time.sleep(0.3)
        
    def assert_overlong_symbols_error_present(self):
        try:
            assert u'Слишком длинно.' in self.driver.find_element(*EquipmentSectionLocators.overLongModelError).text
            print 'Error for Model symbols'
        except:
            print 'for Model symbols is OK'
        
        try:
            assert u'Слишком длинно.' in self.driver.find_element(*EquipmentSectionLocators.overLongManufacturerError).text
            print 'Error for Manufacturer symbols'
        except:
            print 'for Manufacturer symbols is OK'
            
    def assert_empty_fields_error_present(self):
        try:
            assert u'Забыли указать.' in self.driver.find_element(*EquipmentSectionLocators.overLongModelError).text
            print 'Error for Model symbols'
        except:
            print 'for Model symbols is OK'
        
        try:
            assert u'Забыли указать.' in self.driver.find_element(*EquipmentSectionLocators.overLongManufacturerError).text
            print 'Error for Manufacturer symbols'
        except:
            print 'for Manufacturer symbols is OK'
        
    
