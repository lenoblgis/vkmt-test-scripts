# -*- coding: utf-8 -*-
'''
@author: smirnov
'''

import unittest
from selenium import webdriver
from AccountPage import LoginPage
from TripPage import TripPage
from AccountPage import AccountPage
import xlrd
import random
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

binary = FirefoxBinary(r"C:\Program Files\Mozilla Firefox\firefox.exe")

class TestTripSection(unittest.TestCase):

    def setUp(self):
        #self.driver = webdriver.Chrome()
        self.driver = webdriver.Firefox(firefox_binary=binary)
        self.driver.get('https://vkmt-test.lenoblgis.ru/#/login')
        self.driver.implicitly_wait(5)
        self.driver.maximize_window() # max browser window size
        
    def tearDown(self):
        self.driver.quit()
        
    def testTripCreationForDB(self):
        lgn = LoginPage(self.driver)
        trp = TripPage(self.driver)
        acc = AccountPage(self.driver)
        
        lgn.enter_username_password('admin', '123')
        
        for i in range(100):
            Emloyee = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(4))
            acc.click_account_section_button()
            acc.create_account(Emloyee, Emloyee, Emloyee, Emloyee, Emloyee)
            trp.create_trip(Emloyee, u'г Москва', u'Февраль 2017', '1000', '1000', '1000')
