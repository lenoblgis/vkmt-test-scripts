# -*- coding: utf-8 -*-
'''
@author: smirnov
'''
import unittest
from selenium import webdriver
from ProjectPage import ProjectPage
from CompanyPage import CompanySection
from AccountPage import LoginPage
import xlrd
import random

class TestProjectSection(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://vkmt-test.lenoblgis.ru/#/login')
        self.driver.implicitly_wait(5)
        self.driver.maximize_window() # max browser window size
        
    def tearDown(self):
        self.driver.close()
        
    def testProjectCreation(self):
        lgn = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        prj = ProjectPage(self.driver)
        
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Project_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        lgn.enter_username_password('admin', '123')
        
        #for i in range(6): 
        rownum = 1
        rows = sheet1.row_values(rownum)
        cmn.create_short_company(rows[1], rows[2])
        if rows[4]: cmn.create_short_company(rows[3], rows[4])
        prj.create_project(rows[0], rows[2], rows[4], rows[6], rows[8], rows[10], rows[15], rows[12], rows[13])
        for i in range(1):
            prj.add_atachment_to_the_project(rows[0], rows[14])
            prj.assert_project_info_in_card(rows[0], rows[2], rows[4], rows[5], rows[7], rows[10], rows[11], rows[12], rows[13])
            
        cmn.delete_company_after_test()
        cmn.delete_company_after_test()
        prj.delete_project_after_test()
            
    def testProjectModifying(self):
        lgn = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        prj = ProjectPage(self.driver)
        
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Project_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        lgn.enter_username_password('admin', '123')
        
        rownum = 1 
        rows = sheet1.row_values(rownum)
        cmn.create_short_company(rows[1], rows[2])
        if rows[4]: cmn.create_short_company(rows[3], rows[4])
        
        rownum = 2
        rows = sheet1.row_values(rownum)
        cmn.create_short_company(rows[1], rows[2])
        if rows[4]: cmn.create_short_company(rows[3], rows[4])
        prj.create_project(rows[0], rows[2], rows[4], rows[6], rows[8], rows[10], rows[15], rows[12], rows[13])
        
        rownum = 1 
        rows = sheet1.row_values(rownum)
        prj.modify_project(rows[0], rows[2], rows[4], rows[6], rows[8], rows[10], rows[15], rows[12], rows[13])
        prj.assert_project_info_in_card(rows[0], rows[2], rows[4], rows[5], rows[7], rows[10], rows[11], rows[12], rows[13])
        
        cmn.delete_company_after_test()
        cmn.delete_company_after_test()
        cmn.delete_company_after_test()
        cmn.delete_company_after_test()
        prj.delete_project_after_test()
        
    def testProjectVersionCreation(self):
        lgn = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        prj = ProjectPage(self.driver)
        
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Project_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        lgn.enter_username_password('admin', '123')
        
        rownum = 1 
        rows = sheet1.row_values(rownum)
        cmn.create_short_company(rows[1], rows[2])
        if rows[4]: cmn.create_short_company(rows[3], rows[4])
        
        rownum = 2
        rows = sheet1.row_values(rownum)
        cmn.create_short_company(rows[1], rows[2])
        if rows[4]: cmn.create_short_company(rows[3], rows[4])
        prj.create_project(rows[0], rows[2], rows[4], rows[6], rows[8], rows[10], rows[15], rows[12], rows[13])
        
        rownum = 1 
        rows = sheet1.row_values(rownum)
        prj.modify_project(rows[0], rows[2], rows[4], rows[6], rows[8], rows[10], rows[15], rows[12], rows[13])
        
        rownum = 2 
        rows = sheet1.row_values(rownum)
        prj.open_previous_version()
        prj.assert_project_info_in_card(rows[0], rows[2], rows[4], rows[5], rows[7], rows[10], rows[11], rows[12], rows[13])
        
        cmn.delete_company_after_test()
        cmn.delete_company_after_test()
        prj.delete_project_after_test()
        
    def testProjectVersionRecovery(self):
        lgn = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        prj = ProjectPage(self.driver)
        
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Project_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        lgn.enter_username_password('admin', '123')
        
        rownum = 1 
        rows = sheet1.row_values(rownum)
        cmn.create_short_company(rows[1], rows[2])
        if rows[4]: cmn.create_short_company(rows[3], rows[4])
        
        rownum = 2
        rows = sheet1.row_values(rownum)
        cmn.create_short_company(rows[1], rows[2])
        if rows[4]: cmn.create_short_company(rows[3], rows[4])
        prj.create_project(rows[0], rows[2], rows[4], rows[6], rows[8], rows[10], rows[15], rows[12], rows[13])
        
        rownum = 1 
        rows = sheet1.row_values(rownum)
        prj.modify_project(rows[0], rows[2], rows[4], rows[6], rows[8], rows[10], rows[15], rows[12], rows[13])
        prj.open_previous_version()
        prj.recover_previous_version()
        
        rownum = 2
        rows = sheet1.row_values(rownum)
        prj.assert_project_info_in_card(rows[0], rows[2], rows[4], rows[5], rows[7], rows[10], rows[11], rows[12], rows[13])      
        
        cmn.delete_company_after_test()
        cmn.delete_company_after_test()
        prj.delete_project_after_test()
        
    def testProjectDeleting(self):
        lgn = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        prj = ProjectPage(self.driver)
        
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Project_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        lgn.enter_username_password('admin', '123')
        
        rownum = 1
        rows = sheet1.row_values(rownum)
        cmn.create_short_company(rows[1], rows[2])
        prj.create_project(rows[0], rows[2], rows[4], rows[6], rows[8], rows[10], rows[15], rows[12], rows[13])
        prj.delete_project()
        prj.assert_if_project_deleted(rows[0])
        
        cmn.delete_company_after_test()
        
    def testContextProjectSearchByName(self):
        lgn = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        prj = ProjectPage(self.driver)
        
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Project_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        lgn.enter_username_password('admin', '123')
        
        rownum = 4
        rows = sheet1.row_values(rownum)
        cmn.create_short_company(rows[1], rows[2])
        prj.create_project(rows[0], rows[2], rows[4], rows[6], rows[8], rows[10], rows[15], rows[12], rows[13])
        prj.search_project_by_field(rows[0], rows[0])
        
        cmn.delete_company_after_test()
        prj.delete_project_after_test()
        
    def testContextProjectSearchByRegistryNumber(self):
        lgn = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        prj = ProjectPage(self.driver)
        
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Project_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        lgn.enter_username_password('admin', '123')
        
        rownum = 4
        rows = sheet1.row_values(rownum)
        cmn.create_short_company(rows[1], rows[2])
        prj.create_project(rows[0], rows[2], rows[4], rows[6], rows[8], rows[10], rows[15], rows[12], rows[13])
        prj.search_project_by_field(rows[9], rows[0])
        
        cmn.delete_company_after_test()
        prj.delete_project_after_test()
        
    def testMaxSymbolInProjectCreationForm(self):
        lgn = LoginPage(self.driver)
        prj = ProjectPage(self.driver)
        
        lgn.enter_username_password('admin', '123')
        
        print '\nFor 255, 50, 50, 255, 255, 255: \n _______________'
        prj.fill_project_creation_form('N'*255, 'R'*50, 'R'*50, 'F'*255, 'E'*255, 'C'*255)
        prj.assert_overlong_symbols_error_present()
        
    def testMaxPlus1SymbolInProjectCreationForm(self):
        lgn = LoginPage(self.driver)
        prj = ProjectPage(self.driver)
        
        lgn.enter_username_password('admin', '123')
        
        print '\nFor 256, 51, 51, 256, 256, 256: \n _______________'
        prj.fill_project_creation_form('N'*256, 'R'*51, 'R'*51, 'F'*256, 'E'*256, 'C'*256)
        prj.assert_overlong_symbols_error_present()
        
    def testMaxMinus1SymbolInProjectCreationForm(self):
        lgn = LoginPage(self.driver)
        prj = ProjectPage(self.driver)
        
        lgn.enter_username_password('admin', '123')
        
        print '\nFor 254, 49, 49, 254, 254, 254: \n _______________'
        prj.fill_project_creation_form('N'*254, 'R'*49, 'R'*49, 'F'*254, 'E'*254, 'C'*254)
        prj.assert_overlong_symbols_error_present()
        
    def testProjectCreationForDB(self):
        lgn = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        prj = ProjectPage(self.driver)
        
        lgn.enter_username_password('test', '123')
        
        for i in range(5):
            CompanyName = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(8))
            CompanyShortName = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(8))
            ProjectName = ''.join(random.choice(u'абвгдеёжзикл') for _ in range(8))
            cmn.create_short_company(CompanyName, CompanyShortName)
            ProjectType = random.randint(1, 3)
            ProjectStatus = random.randint(1, 6)
            FinancingSource = random.randint(1, 3)
            prj.create_project(ProjectName, CompanyShortName, ' ', ProjectType, ProjectStatus, 'rows[10]', FinancingSource, 'rows[12]', 'rows[13]')
            
    def testProjectCreationForDemo(self):
        lgn = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        prj = ProjectPage(self.driver)
        
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Project_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        lgn.enter_username_password('admin', '123')
        
        for i in range(6): 
            rownum = 1
            rows = sheet1.row_values(rownum)
            #cmn.create_short_company(rows[1], rows[2])
            #if rows[4]: cmn.create_short_company(rows[3], rows[4])
            prj.create_project(rows[0], rows[2], rows[4], rows[6], rows[8], rows[10], rows[15], rows[12], rows[13])
            for i in range(1):
                prj.add_atachment_to_the_project(rows[0], rows[14])
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        