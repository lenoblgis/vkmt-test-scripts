# -*- coding: utf-8 -*-
'''
@author: smirnov
'''

from locators import ActionSectionLocators
import time
import random

class ActionPage():
    
    def __init__(self, driver):
        self.driver = driver
        
    def action_type_choosing_in_section(self, ActionType):
        time.sleep(0.5)
        self.driver.find_element_by_xpath('/html/body/md-content/div[2]/md-fab-speed-dial/md-fab-actions/a[%s]' % ActionType).click()
        time.sleep(0.5)
        
    def action_type_choosing_in_card(self, ActionType):
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.actionTypeInCard).click()
        time.sleep(0.5)
        try:
            self.driver.find_element_by_xpath('/html/body/div[4]/md-select-menu/md-content/md-option[%s]' % ActionType).click() 
        except:     
            self.driver.find_element_by_xpath('/html/body/div[5]/md-select-menu/md-content/md-option[%s]' % ActionType).click()
        time.sleep(0.5)
        
    def action_status_choosing(self, Status):
        time.sleep(0.5)
        self.driver.find_element_by_xpath('/html/body/div[5]/md-select-menu/md-content/md-option[%s]/div' % Status).click()
        time.sleep(0.5)
        
    def action_status_choosing_in_modification(self, Status):
        time.sleep(0.5)
        try: 
            self.driver.find_element_by_xpath('/html/body/div[6]/md-select-menu/md-content/md-option[%s]' % Status).click()
        except: 
            self.driver.find_element_by_xpath('/html/body/div[5]/md-select-menu/md-content/md-option[%s]' % Status).click()
        time.sleep(0.5)
        
    def action_status_choosing_by_status_text(self, Status):
        time.sleep(0.5)
        self.driver.find_element_by_xpath("//*[contains(text(), '%s')]" % Status).click()
        time.sleep(0.5)
        
    def action_direction_choosing(self, Direction):
        self.driver.find_element(*ActionSectionLocators.directionButton).click()
        time.sleep(0.5)
        self.driver.find_element_by_xpath('/html/body/div[6]/md-select-menu/md-content/md-option[%s]' % Direction).click()
        time.sleep(0.5)
        
    def action_direction_choosing_in_modification(self, Direction):
        self.driver.find_element(*ActionSectionLocators.directionButton).click()
        time.sleep(0.5)
        try: 
            self.driver.find_element_by_xpath('/html/body/div[7]/md-select-menu/md-content/md-option[%s]' % Direction).click()
        except: 
            self.driver.find_element_by_xpath('/html/body/div[8]/md-select-menu/md-content/md-option[%s]' % Direction).click()
        time.sleep(0.5)
        
    def create_action(self, ActionType, Direction, Company, Status, Object):
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.actionSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.createActionButton).click()
        time.sleep(0.5)
        ActionPage.action_type_choosing_in_section(self, ActionType)
        time.sleep(1)
        self.driver.find_element(*ActionSectionLocators.companyField).click()
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.searchCompanyField).send_keys(Company)
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.firstStringInsearchCompanyResults).click()
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.statusButton).click()
        time.sleep(0.5)
        ActionPage.action_status_choosing(self, Status) 
        time.sleep(0.5)
        
        if int(ActionType) in (2, 3):
            ActionPage.action_direction_choosing(self, Direction)
            time.sleep(0.5)
            
        self.driver.find_element(*ActionSectionLocators.actionObjectField).send_keys(Object)
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.saveActionDataButton).click()
        time.sleep(1)
        
    def create_action_via_company(self, ActionType, Company, Status, Direction, Object):
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.createActionInCompanyButton).click()
        ActionPage.action_type_choosing_in_card(self, ActionType)
        #self.driver.find_element(*ActionSectionLocators.companyField).click()
        #time.sleep(0.5)
        #self.driver.find_element(*ActionSectionLocators.searchCompanyField).send_keys(Company)
        #time.sleep(0.5)
        #self.driver.find_element(*ActionSectionLocators.firstStringInSearchCompanyResultsViaCompany).click()
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.statusButton).click()
        time.sleep(0.5)
        ActionPage.action_status_choosing_in_modification(self, Status)
        if int(ActionType) in (2, 3):
            ActionPage.action_direction_choosing_in_modification(self, Direction)
            time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.actionObjectField).clear()
        self.driver.find_element(*ActionSectionLocators.actionObjectField).send_keys(Object)
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.createActionViaCompanyButton).click()
        time.sleep(0.5)
        
    def create_action_via_contact(self, ActionType, Status, Object):
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.createActionInCompanyButton).click()
        ActionPage.action_type_choosing_in_card(self, ActionType)
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.statusButton).click()
        time.sleep(0.5)
        ActionPage.action_status_choosing_in_modification(self, Status)
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.actionObjectField).clear()
        self.driver.find_element(*ActionSectionLocators.actionObjectField).send_keys(Object)
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.createActionViaContactButton).click()
        time.sleep(1)
        
    def assert_action_info_in_list(self, ActionType, CompanyShortName, Object):
        self.driver.find_element(*ActionSectionLocators.actionSectionButton).click()
        time.sleep(0.5)
        assert ActionType in self.driver.find_element(*ActionSectionLocators.actionInfoInList).text
        if CompanyShortName: assert CompanyShortName in self.driver.find_element(*ActionSectionLocators.actionInfoInList).text
        assert Object in self.driver.find_element(*ActionSectionLocators.actionInfoInList).text
        time.sleep(1)
        
    def assert_action_info_in_card(self, ActionType, CompanyName, Status, Object):
        time.sleep(2)
        try: self.driver.find_element(*ActionSectionLocators.openFirstActionInListButton).click() 
        except: self.driver.find_element(*ActionSectionLocators.openFirstActionInCompanyButton).click()
        time.sleep(0.5)
        assert ActionType in self.driver.find_element(*ActionSectionLocators.actionInfoInCard).text
        if CompanyName: assert CompanyName in self.driver.find_element(*ActionSectionLocators.actionInfoInCard).text
        assert CompanyName in self.driver.find_element(*ActionSectionLocators.actionInfoInCard).text
        assert Status in self.driver.find_element(*ActionSectionLocators.actionInfoInCard).text
        assert Object in self.driver.find_element(*ActionSectionLocators.actionInfoInCard).text
        
    def modify_action(self, ActionType, Direction, Company, Status, Object):
        self.driver.find_element(*ActionSectionLocators.actionSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.openFirstActionInListButton).click()
        time.sleep(0.5)
        ActionPage.action_type_choosing_in_card(self, ActionType)
        self.driver.find_element(*ActionSectionLocators.companyField).click()
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.searchCompanyField).send_keys(Company)
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.firstStringInsearchCompanyResultsInModification).click()
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.statusButton).click()
        time.sleep(0.5)
        ActionPage.action_status_choosing_in_modification(self, Status)
        if int(ActionType) in (2, 3):
            ActionPage.action_direction_choosing_in_modification(self, Direction)
            time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.actionObjectField).clear()
        self.driver.find_element(*ActionSectionLocators.actionObjectField).send_keys(Object)
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.modifyActionButton).click()
        time.sleep(1)
        
    def delete_action_after_test(self):
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.actionSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.openFirstActionInListButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.deleteActionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.deleteConfirmationButton).click()
        time.sleep(0.5)
        
    def delete_action(self):
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.openFirstActionInListButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.deleteActionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.deleteConfirmationButton).click()
        time.sleep(0.5)
        
    def assert_action_deleting(self, Object):
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.actionSectionButton).click()
        time.sleep(0.5)
        try: assert Object in self.driver.find_element(*ActionSectionLocators.actionInfoInList).text()
        except: print 'For testActionDeleting. No current Action in list. Action has been removed'
        
    def fill_action_creation_form(self, Object):
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.actionSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.createActionButton).click()
        time.sleep(0.5)
        self.driver.find_element_by_xpath('/html/body/md-content/div[2]/md-fab-speed-dial/md-fab-actions/a[1]').click()
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.actionObjectField).send_keys(Object)
        time.sleep(1)
        self.driver.find_element(*ActionSectionLocators.fullTextSearchField).click()
        time.sleep(1)
        
    def assert_overlong_symbols_error_present(self):
        try:
            assert u'Слишком длинно.' in self.driver.find_element(*ActionSectionLocators.overLongObjectError).text
            print 'Error for Object symbols'
        except:
            print 'For Object symbols is OK'
            
    def create_random_action(self):
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.actionSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.createActionButton).click()
        time.sleep(0.5)
        ActionPage.action_type_choosing_in_section(self, random.randint(1, 5))
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.companyField).click()
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.secondStringInsearchCompanyResults).click()
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.statusButton).click()
        time.sleep(0.5)
        ActionPage.action_status_choosing(self, random.randint(1, 2))
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.actionObjectField).send_keys(''.join(random.choice('abcdefghijklmnopqrstuvwxyz ') for _ in range(2000)))
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.saveActionDataButton).click()
        
    def open_action_in_company(self):
        time.sleep(0.5)
        self.driver.find_element(*ActionSectionLocators.openFirstActionInCompanyButton).click()
        time.sleep(0.5)
        
        
        
        
                                
        
        
        
        
        
        
        
        
        