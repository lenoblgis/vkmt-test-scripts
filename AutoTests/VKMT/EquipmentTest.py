# -*- coding: utf-8 -*-
'''
@author: smirnov
'''
import unittest
from selenium import webdriver
from EquipmentPage import EquipmentPage
from AccountPage import LoginPage
import xlrd
equipmentCount = 1
import random


class TestEquipmentSection(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://vkmt-test.lenoblgis.ru/#/login')
        self.driver.implicitly_wait(5)
        self.driver.maximize_window() # max browser window size
        
    def tearDown(self):
        self.driver.close()

    def testEquipmentCreation(self):
        equipmentPage = EquipmentPage(self.driver)
        loginPage = LoginPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Eqiupment_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = (1)
        rows = sheet1.row_values(rownum)
        equipmentPage.creat_equipment(rows[2], rows[4], rows[0], rows[1])
        equipmentPage.assert_equipment_info_in_list(rownum)
        equipmentPage.open_current_equipment_details(rows[2])
        equipmentPage.assert_equipment_info_in_card(rownum)
        equipmentPage.delete_equipment_after_test()
            
    def testEquipmentModification(self):
        equipmentPage = EquipmentPage(self.driver)
        loginPage = LoginPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Eqiupment_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 4
        rows = sheet1.row_values(rownum)
        equipmentPage.creat_equipment(rows[2], rows[4], rows[0], rows[1])
        
        rownum = 5
        rows = sheet1.row_values(rownum)
        equipmentPage.modify_equipment(rows[2], rows[4], rows[0], rows[1])
        equipmentPage.assert_equipment_info_in_card(rownum)
        
        equipmentPage.delete_equipment_after_test()
        
    def testEquipmentDeleting(self):
        equipmentPage = EquipmentPage(self.driver)
        loginPage = LoginPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Eqiupment_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 3
        rows = sheet1.row_values(rownum)
        equipmentPage.creat_equipment(rows[2], rows[4], rows[0], rows[1])
        
        equipmentPage.delete_equipment_after_test()
        equipmentPage.assert_if_equipment_delete(rows[2])
        
    def testContextEquipmentSearchByModel(self):
        equipmentPage = EquipmentPage(self.driver)
        loginPage = LoginPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Eqiupment_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 3
        rows = sheet1.row_values(rownum)
        equipmentPage.creat_equipment(rows[2], rows[4], rows[0], rows[1])
        
        equipmentPage.search_equipment_by_field(rows[2])
        
        equipmentPage.delete_equipment_after_test()
        
    def testMaxSymbolInEquipmentCreationForm(self):
        equipmentPage = EquipmentPage(self.driver)
        loginPage = LoginPage(self.driver)
        
        loginPage.enter_username_password('admin', '123')
        
        print '\nFor 255, 255: \n _______________'
        equipmentPage.fill_equipment_creation_form('M' * 255, 'M' * 255)
        equipmentPage.assert_overlong_symbols_error_present()
        
    def testMaxSymbolPlus1InEquipmentCreationForm(self):
        equipmentPage = EquipmentPage(self.driver)
        loginPage = LoginPage(self.driver)
        
        loginPage.enter_username_password('admin', '123')
        
        print '\nFor 256, 256: \n _______________'
        equipmentPage.fill_equipment_creation_form('M' * 256, 'M' * 256)
        equipmentPage.assert_overlong_symbols_error_present()
        
    def testMaxSymbolMinus1InEquipmentCreationForm(self):
        equipmentPage = EquipmentPage(self.driver)
        loginPage = LoginPage(self.driver)
        
        loginPage.enter_username_password('admin', '123')
        
        print '\nFor 254, 254: \n _______________'
        equipmentPage.fill_equipment_creation_form('M' * 254, 'M' * 254)
        equipmentPage.assert_overlong_symbols_error_present()
    
    def testZeroSymbolsInEquipmentCreationForm(self):
        equipmentPage = EquipmentPage(self.driver)
        loginPage = LoginPage(self.driver)
        
        loginPage.enter_username_password('admin', '123')
        
        print '\nFor 0, 0: \n _______________'
        equipmentPage.fill_equipment_creation_form('M' * 0, 'M' * 0)
        equipmentPage.assert_empty_fields_error_present()
        
    def testEquipmentCreationForDB(self):
        equipmentPage = EquipmentPage(self.driver)
        loginPage = LoginPage(self.driver)
        
        loginPage.enter_username_password('admin', '123')
        
        for i in range(180):
            model = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(10))
            manufacturer = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(10))
            types = 1
            kinds = random.randint(1, 12)
            
            equipmentPage.creat_equipment(model, manufacturer, kinds, types)
            
    def testEquipmentCreationForDemo(self):
        equipmentPage = EquipmentPage(self.driver)
        loginPage = LoginPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Eqiupment_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        for i in range(24): # for filling base
            rownum = (1)
            rows = sheet1.row_values(rownum)
            equipmentPage.creat_equipment(rows[2], rows[4], rows[0], rows[1])



        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()