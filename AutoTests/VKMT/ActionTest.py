# -*- coding: utf-8 -*-
'''
@author: smirnov
'''
import unittest
from selenium import webdriver
from ActionPage import ActionPage
from CompanyPage import CompanySection
from AccountPage import LoginPage
from ContactPage import ContactPage
import xlrd

class TestActionSection(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://vkmt-test.lenoblgis.ru/#/login')
        self.driver.implicitly_wait(5)
        self.driver.maximize_window() # max browser window size
        
    def tearDown(self):
        self.driver.close()
        
    def testActionCreation(self):
        loginPage = LoginPage(self.driver)
        companyPage = CompanySection(self.driver)
        actionPage = ActionPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Action_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        #for i in range(4):
        rownum = (1)
        rows = sheet1.row_values(rownum)
        companyPage.create_short_company(rows[4], rows[5])
        actionPage.create_action(rows[1], rows[3], rows[5], rows[7], rows[8])
        
        actionPage.assert_action_info_in_list(rows[0], rows[5], rows[8])
        
        actionPage.delete_action_after_test()
        companyPage.delete_company_after_test()
    
    def testActionCreationViaCompany(self):
        lgn = LoginPage(self.driver)
        cmn = CompanySection(self.driver)
        act = ActionPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Action_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        lgn.enter_username_password('admin', '123')
        
        rownum = (3)
        rows = sheet1.row_values(rownum)
        cmn.create_short_company('Name', 'ShortName')
        act.create_action_via_company(rows[1], 'ShortName', rows[7], rows[3], rows[8])
        act.open_action_in_company()
        act.assert_action_info_in_list(rows[0], 'ShortName', rows[8])
        
    def testActionCreationViaContact(self):
        lgn = LoginPage(self.driver)
        cnt = ContactPage(self.driver)
        act = ActionPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Contact_DDT.xlsx')
        wb1 = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Action_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        sheet2 = wb1.sheet_by_index(0)
        
        lgn.enter_username_password('admin', '123')
        
        rownum = 2
        rows = sheet1.row_values(rownum)
        rows1 = sheet2.row_values(rownum)
        cnt.creat_contact(rows[0], rows[1], rows[2], rows[3], rows[4], rows[5], rows[6], rows[7])
        act.create_action_via_contact(rows1[1], rows1[7], rows1[8])
        act.assert_action_info_in_list(rows1[1], '', rows1[8])
         
    def testActionModification(self):
        loginPage = LoginPage(self.driver)
        companyPage = CompanySection(self.driver)
        actionPage = ActionPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Action_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 4
        rows = sheet1.row_values(rownum)
        companyPage.create_short_company(rows[4], rows[5])
        actionPage.create_action(rows[1], rows[3], rows[5], rows[7], rows[8])
        
        rownum = 2
        rows = sheet1.row_values(rownum)
        companyPage.create_short_company(rows[4], rows[5])
        actionPage.modify_action(rows[1], rows[3], rows[5], rows[7], rows[8])
        actionPage.assert_action_info_in_list(rows[0], rows[5], rows[8])
        
        actionPage.delete_action_after_test()
        companyPage.delete_company_after_test()
        companyPage.delete_company_after_test()
        
    def testActionDeleting(self):
        loginPage = LoginPage(self.driver)
        companyPage = CompanySection(self.driver)
        actionPage = ActionPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Action_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        rownum = 2
        rows = sheet1.row_values(rownum)
        companyPage.create_short_company(rows[4], rows[5])
        actionPage.create_action(rows[1], rows[3], rows[5], rows[7], rows[8])
        actionPage.delete_action()
        actionPage.assert_action_deleting(rows[8])
        
        companyPage.delete_company_after_test()
        
    def testMaxSymbolsInActionCreationForm(self):
        loginPage = LoginPage(self.driver)
        actionPage = ActionPage(self.driver)
        
        loginPage.enter_username_password('admin', '123')
        
        print '\nFor 2000: \n _______________'
        actionPage.fill_action_creation_form('O' * 2000)
        actionPage.assert_overlong_symbols_error_present()
        
    def testMaxPlus1SymbolsInActionCreationForm(self):
        loginPage = LoginPage(self.driver)
        actionPage = ActionPage(self.driver)
        
        loginPage.enter_username_password('admin', '123')
        
        print '\nFor 2001: \n _______________'
        actionPage.fill_action_creation_form('O' * 2001)
        actionPage.assert_overlong_symbols_error_present()
        
    def testMaxMinus1SymbolsInActionCreationForm(self):
        loginPage = LoginPage(self.driver)
        actionPage = ActionPage(self.driver)
        
        loginPage.enter_username_password('admin', '123')
        
        print '\nFor 1999: \n _______________'
        actionPage.fill_action_creation_form('O' * 1999)
        actionPage.assert_overlong_symbols_error_present()
         
    def testActionCreationForBD(self):
        loginPage = LoginPage(self.driver)    
        actionPage = ActionPage(self.driver)
            
        loginPage.enter_username_password('admin', '123')
        
        for i in range(70):
            actionPage.create_random_action()
            
    def testActionCreationForDemo(self):
        loginPage = LoginPage(self.driver)
        companyPage = CompanySection(self.driver)
        actionPage = ActionPage(self.driver)
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Action_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')
        
        for i in range(4):
            rownum = (i+1)
            rows = sheet1.row_values(rownum)
            #companyPage.create_short_company(rows[4], rows[5])
            actionPage.create_action(rows[1], rows[3], rows[5], rows[7], rows[8])

        
             
        
        
        
        
        
        
        
        
        
        