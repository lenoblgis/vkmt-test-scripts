# -*- coding: utf-8 -*-
'''
@author: smirnov
'''
import unittest
from selenium import webdriver
from AccountPage import LoginPage, AccountPage, ReturnDefaultData
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import xlrd

binary = FirefoxBinary(r"C:\Program Files\Mozilla Firefox\firefox.exe")

class TestLoginForm(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://vkmt-demo.lenoblgis.ru/#/login')
        self.driver.implicitly_wait(5)
        self.driver.maximize_window() # max browser window size

    def tearDown(self):
        self.driver.close()

    def testLogin(self):
        loginPage = LoginPage(self.driver)
        
        loginPage.enter_username_password('admin', '123')
        loginPage.click_login_button()
        loginPage.is_login_error_present()
        
class TestAccountForms(unittest.TestCase):
    
    def setUp(self):
        self.driver = webdriver.Firefox(firefox_binary=binary)
        self.driver.get('https://vkmt-test.lenoblgis.ru/#/login')
        self.driver.implicitly_wait(5)
        self.driver.maximize_window() # max browser window size

    def tearDown(self):
        self.driver.quit()
    
    def testAdminAccountModifying(self):
        loginPage = LoginPage(self.driver)
        accountPage = AccountPage(self.driver)
        
        loginPage.enter_username_password('admin', '123')

        accountPage.click_account_section_button()
        accountPage.click_admin_account()
        accountPage.click_modifying_button_on_quick_panel()
        accountPage.enter_account_data(' ', u'Администратор', u'Администратор')
        accountPage.click_save_account_button()
        
    def testMoreThen32Password(self):
        loginPage = LoginPage(self.driver)
        accountPage = AccountPage(self.driver)
        returnAccountData = ReturnDefaultData(self.driver)
        
        loginPage.enter_username_password('admin', '123')

        accountPage.assert_change_admin_password('p' * 33, 'p' * 33)
        returnAccountData.returnDefaultAdminPassword()
        
    def testBigPassword(self):
        loginPage = LoginPage(self.driver)
        accountPage = AccountPage(self.driver)
        returnAccountData = ReturnDefaultData(self.driver)

        loginPage.enter_username_password('admin', '123')
      
        accountPage.assert_change_admin_password('p' * 100, 'p' * 100)
        returnAccountData.returnDefaultAdminPassword()
        
    def testEmptyPassword(self):
        loginPage = LoginPage(self.driver)
        accountPage = AccountPage(self.driver)
        returnAccountData = ReturnDefaultData(self.driver)

        loginPage.enter_username_password('admin', '123')
      
        accountPage.assert_change_admin_password('', '')
        returnAccountData.returnDefaultAdminPassword()
        
    def testOneSymbolPassword(self):
        loginPage = LoginPage(self.driver)
        accountPage = AccountPage(self.driver)
        returnAccountData = ReturnDefaultData(self.driver)

        loginPage.enter_username_password('admin', '123')
     
        accountPage.assert_change_admin_password('1', '1') 
        returnAccountData.returnDefaultAdminPassword() 
        
    def test31SymbolPassword(self):
        loginPage = LoginPage(self.driver)
        accountPage = AccountPage(self.driver)
        returnAccountData = ReturnDefaultData(self.driver)

        loginPage.enter_username_password('admin', '123')
        
        accountPage.assert_change_admin_password('p' * 31, 'p' * 31)    
        returnAccountData.returnDefaultAdminPassword()  
        
    def testSpecialsSymbolPassword(self):
        loginPage = LoginPage(self.driver)
        accountPage = AccountPage(self.driver)
        returnAccountData = ReturnDefaultData(self.driver)

        loginPage.enter_username_password('admin', '123' )
        
        accountPage.assert_change_admin_password('!@#$%^&*()_+~', '!@#$%^&*()_+~') 
        returnAccountData.returnDefaultAdminPassword() 
               
    def testAccountCreationAndModification(self):
        loginPage = LoginPage(self.driver)
        accountPage = AccountPage(self.driver) 
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Accounts_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')  
            
        #for i in range(account_amount):
        rownum=(3)
        rows = sheet1.row_values(rownum)
        accountPage.click_account_section_button()
        accountPage.create_account(rows[0], rows[1], rows[2], rows[3], rows[4])
        accountPage.modify_account(rows[5], rows[6], rows[7])
        accountPage.assert_account_info(rownum)
            
    def testSymbolAmountInCreationForm(self):
        loginPage = LoginPage(self.driver)
        accountPage = AccountPage(self.driver)
         
        loginPage.enter_username_password('admin', '123')
        
        accountPage.empty_fields_assert()
        accountPage.large_symbols_count_assert()   
        
    def testAccountCreationForDemo(self):
        loginPage = LoginPage(self.driver)
        accountPage = AccountPage(self.driver) 
        wb = xlrd.open_workbook('C:\Users\smirnov\Tests\AutoTests\VKMT\TestData\Accounts_DDT.xlsx')
        sheet1 = wb.sheet_by_index(0)
        
        loginPage.enter_username_password('admin', '123')  
        
        accountPage.click_account_section_button()
        accountPage.click_admin_account()
        accountPage.click_modifying_button_on_quick_panel()
        accountPage.enter_account_data(' ', u'Администратор', u'Администратор')
        accountPage.click_save_account_button()
            
        for i in range(2):
            rownum=(i+2)
            rows = sheet1.row_values(rownum)
            accountPage.click_account_section_button()
            accountPage.create_account(rows[0], rows[1], rows[2], rows[3], rows[4])
            accountPage.modify_account(rows[5], rows[6], rows[7])
            accountPage.assert_account_info(rownum)           
            
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()