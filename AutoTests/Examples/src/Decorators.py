'''
@author: smirnov
'''

def new_decorator(func):
    
    def wrapTheFunction():
        print 'Before'
        func()
        print 'After'
        
    return wrapTheFunction

def function_for_decorating():
    print 'For decorating'
    
@new_decorator
def function_for_decorating():
    print 'For decorating'

function_for_decorating()


