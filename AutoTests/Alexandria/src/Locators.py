# -*- coding: utf-8 -*-
'''
@author: smirnov
'''

import unittest
from selenium.webdriver.common.by import By

class LoginPageLocators(object):
    loginField = (By.XPATH, '//*[@ng-model="login.name"]')
    passwordField = (By.XPATH, '//*[@ng-model="login.password"]')
    signInButton = (By.XPATH, '//*[@ng-click="signIn(login);"]')
    
class PointPageLocators(object):
    pointPageButton = (By.XPATH, '/html/body/header/div/nav/a[4]')
    addPointButton = (By.XPATH, '//*[@ng-click="addPoint()"]')
    pointIdField = (By.XPATH, '//*[@ng-model="point.id"]')
    pointNumberField = (By.XPATH, '//*[@ng-model="point.number"]')
    pointDescriptionField = (By.XPATH, '//*[@ng-model="point.description"]')
    savePointButton = (By.XPATH, '//*[@ng-click="save();"]')
    
class UserSectionLocators(object):
    accountSectionButton = (By.XPATH, '/html/body/header/div/nav/a[6]')
    createNewAccount = (By.XPATH, '//*[@ng-click="addAccount()"]')
    userRoleSelector = (By.XPATH, '//*[@ng-model="account.role"]')
    executerRoleStroke = (By.XPATH, '/html/body/div[3]/md-select-menu/md-content/md-option[3]')
    userLoginField = (By.XPATH, '//*[@ng-model="account.login"]')
    userLastNameField = (By.XPATH, '//*[@ng-model="account.lastName"]')
    userFirstNameField = (By.XPATH, '//*[@ng-model="account.firstName"]')
    userMiddleNameField = (By.XPATH, '//*[@ng-model="account.middleName"]')
    saveAccount = (By.XPATH, '//*[@ng-click="save(account);"]')
    
    
