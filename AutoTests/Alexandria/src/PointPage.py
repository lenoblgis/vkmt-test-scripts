# -*- coding: utf-8 -*-
'''
@author: smirnov
'''

from Locators import PointPageLocators 
import time

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import element_to_be_clickable

class PointPage(object):

    def __init__(self, driver):
        self.driver = driver
        
    def create_point(self, Id, Number, Discription):
        self.driver.find_element(*PointPageLocators.pointPageButton).click()
        time.sleep(0.5)
        self.driver.find_element(*PointPageLocators.addPointButton).click()
        time.sleep(0.5)
        self.driver.find_element(*PointPageLocators.pointIdField).send_keys(Id)
        time.sleep(0.5)
        self.driver.find_element(*PointPageLocators.pointNumberField).send_keys(Number)
        time.sleep(0.5)
        self.driver.find_element(*PointPageLocators.pointDescriptionField).send_keys(Discription)
        time.sleep(0.5)
        self.driver.find_element(*PointPageLocators.savePointButton).click()
        time.sleep(1)
        
        #WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//button[@type='button']"))).click()
        