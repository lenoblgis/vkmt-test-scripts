# -*- coding: utf-8 -*-
'''
@author: smirnov
'''

from Locators import UserSectionLocators
import time

class AccountPage(object):
    
    def __init__(self, driver):
        self.driver = driver
        
    def creat_user(self, login, lastname, firstname, middlename):
        self.driver.find_element(*UserSectionLocators.accountSectionButton).click()
        time.sleep(0.5)
        self.driver.find_element(*UserSectionLocators.createNewAccount).click()
        time.sleep(0.5)
        self.driver.find_element(*UserSectionLocators.userRoleSelector).click()
        time.sleep(0.5)
        self.driver.find_element(*UserSectionLocators.executerRoleStroke).click()
        time.sleep(0.5)
        self.driver.find_element(*UserSectionLocators.userLoginField).send_keys(login)
        self.driver.find_element(*UserSectionLocators.userLastNameField).send_keys(lastname)
        self.driver.find_element(*UserSectionLocators.userFirstNameField).send_keys(firstname)
        self.driver.find_element(*UserSectionLocators.userMiddleNameField).send_keys(middlename)
        time.sleep(0.5)
        self.driver.find_element(*UserSectionLocators.saveAccount).click()
        
        
        
        
        