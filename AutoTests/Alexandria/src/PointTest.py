# -*- coding: utf-8 -*-
'''
@author: smirnov
'''

import unittest
from selenium import webdriver
from PointPage import PointPage
import random
from LoginPage import LoginPage

class TestPointSection(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.get('https://al-test.lenoblgis.ru/login')
        self.driver.implicitly_wait(5)
        self.driver.maximize_window() # max browser window size
        
    def tearDown(self):
        self.driver.close()

    def testPointCreationForDB(self):
        lgn = LoginPage(self.driver)
        pnt = PointPage(self.driver)
        
        lgn.log_in_system('admin', '123')
        
        for i in range(100):
            id = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz ') for _ in range(255))
            number = random.randint(900000000, 999999999)
            pnt.create_point('Id %d' % i, i, 'Description %d' % i)
            
        
        
        