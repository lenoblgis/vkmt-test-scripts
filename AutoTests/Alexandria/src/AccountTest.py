# -*- coding: utf-8 -*-
'''
@author: smirnov
'''

import unittest
from selenium import webdriver
from LoginPage import LoginPage
from AcountPage import AccountPage
import random
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

binary = FirefoxBinary(r"C:\Program Files\Mozilla Firefox\firefox.exe")

class TestAccountSection(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(firefox_binary=binary)
        #self.driver = webdriver.Chrome()
        self.driver.get('https://al-test.lenoblgis.ru/login')
        self.driver.implicitly_wait(5)
        self.driver.maximize_window() # max browser window size
        
    def tearDown(self):
        #self.driver.close()
        self.driver.quit()
        
    def testAccountCreationForDB(self):
        lgn = LoginPage(self.driver)
        acc = AccountPage(self.driver)
        
        lgn.log_in_system('admin', '123')
        
        for i in range(100):
            login = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(5))
            lastname = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(5))
            firstname = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(5))
            middlename = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(5))
            acc.creat_user(login, lastname, firstname, middlename)
        
        
        
        