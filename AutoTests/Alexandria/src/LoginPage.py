# -*- coding: utf-8 -*-
'''
@author: smirnov
'''

import unittest
from Locators import LoginPageLocators 
import time, xlrd

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import element_to_be_clickable

class LoginPage(object):

    def __init__(self, driver):
        self.driver = driver
        
    def log_in_system(self, Login, Password):
        self.driver.find_element(*LoginPageLocators.loginField).send_keys(Login)
        self.driver.find_element(*LoginPageLocators.passwordField).send_keys(Password)
        time.sleep(0.5)
        self.driver.find_element(*LoginPageLocators.signInButton).click()
        time.sleep(1)
        
    #WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//button[@type='button']"))).click()
    
    #def log_in_system2(self, Login, Password):
        #WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@ng-model="login.name"]'))).send_keys(Login)
        
        