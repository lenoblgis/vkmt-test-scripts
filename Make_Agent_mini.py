# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import random

class MakeAgentMini(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.base_url = "https://vkmt-test.lenoblgis.ru/"
        self.driver.maximize_window() # max browser window size
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_make_agent_mini(self):

        driver = self.driver
        driver.get(self.base_url + "/#/login")
        driver.find_element_by_xpath("/html/body/main/div/md-card/md-card-content/form/md-input-container[1]/input").click()
        driver.find_element_by_xpath("/html/body/main/div/md-card/md-card-content/form/md-input-container[1]/input").clear()
        driver.find_element_by_xpath("/html/body/main/div/md-card/md-card-content/form/md-input-container[1]/input").send_keys("admin")
        driver.find_element_by_xpath("/html/body/main/div/md-card/md-card-content/form/md-input-container[2]/input").clear()
        driver.find_element_by_xpath("/html/body/main/div/md-card/md-card-content/form/md-input-container[2]/input").send_keys("123")
        driver.find_element_by_xpath("//button[@type='button']").click()
        for i in range(10):
            company_name = 'Company#%s' % i
            x = random.randint(1, 5)
            driver.find_element_by_link_text(u"Контрагенты").click()
            driver.find_element_by_css_selector("div.md-toolbar-tools > button.md-button.md-ink-ripple").click()
            driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[1]/div[1]/textarea").clear()
            driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[1]/div[1]/textarea").send_keys(company_name)
            time.sleep(0.3)
            driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[2]/input").clear()
            driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[2]/input").send_keys(('Cmp#%s') % i)
            time.sleep(0.3)
            driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[1]/md-input-container[3]/md-select").click()
            time.sleep(0.3)
            driver.find_element_by_xpath("/html/body/div[4]/md-select-menu/md-content/md-option[2]").click()
            time.sleep(0.3)
            driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container[1]/input").clear()
            driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container[1]/input").send_keys(("+7 (111) %s") % random.randint(1000000, 9999999))
            time.sleep(0.3)
            driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container[2]/input").clear()
            driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container[2]/input").send_keys(("+7 (111) %s") % random.randint(1000000, 9999999))
            time.sleep(0.3)
            driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container[3]/input").clear()
            driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container[3]/input").send_keys(("%s@mail.ru") % company_name)
            time.sleep(0.3)
            driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[3]/md-input-container[1]/input").clear()
            driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[3]/md-input-container[1]/input").send_keys(("%s.ru") % (company_name))
            time.sleep(0.3)
            driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[3]/md-input-container[2]/div[1]/textarea").clear()
            driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[3]/md-input-container[2]/div[1]/textarea").send_keys(u"Улица 123")
            time.sleep(0.5)
            driver.find_element_by_xpath("(//button[@type='button'])[8]").click()
            time.sleep(1)
            driver.find_element_by_link_text(u"Контрагенты").click()


    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
