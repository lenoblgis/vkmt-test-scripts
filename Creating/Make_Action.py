# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import random

action_amount = 20


driver = webdriver.Chrome()
driver.implicitly_wait(10)
base_url = "https://vkmt-test.lenoblgis.ru/"
driver.maximize_window() # max browser window size
verificationErrors = []
accept_next_alert = True

def make_action(driver):

    driver.get(base_url + "/#/login")
    driver.find_element_by_xpath("//*[@ng-model=\"login.username\"]").send_keys("Admin")
    driver.find_element_by_xpath("//*[@ng-model=\"login.password\"]").send_keys("123")
    driver.find_element_by_xpath("//button[@type='button']").click()
    time.sleep(0.3)
    for i in range(action_amount):
        type_number = random.randint(1, 4)
        driver.find_element_by_link_text(u"Действия").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/main/div/div[2]/md-card/md-card-content/md-menu/button").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[3]/md-menu-content/md-menu-item[%s]/a" % type_number).click()
        time.sleep(0.8)
        if type_number == 1:
            driver.find_element_by_xpath("//*[@ng-model=\"action.company\"]").click()
            time.sleep(0.3)
            driver.find_element_by_xpath('/html/body/div[3]/md-select-menu/md-content/md-option[%s]' % random.randint(1, 3)).click()
            time.sleep(0.3)
            driver.find_element_by_xpath("//*[@ng-model=\"action.contact\"]").click()
            time.sleep(0.3)
            driver.find_element_by_xpath('/html/body/div[4]/md-select-menu/md-content/md-option[%s]' % random.randint(1, 3)).click()
            time.sleep(0.3)
            driver.find_element_by_xpath('/html/body/main/div/div/md-card/md-card-content/form/ng-include/div/div[3]/md-input-container[1]/md-select/md-select-value').click()
            time.sleep(0.3)
            driver.find_element_by_xpath("/html/body/div[5]/md-select-menu/md-content/md-option[%s]" % random.randint(1, 2)).click()
            time.sleep(0.3)
            driver.find_element_by_xpath("/html/body/main/div/div/md-card/md-card-content/form/ng-include/div/div[3]/md-input-container[2]/div[1]/textarea").send_keys((u"Встреча №%s" % i))
            time.sleep(0.3)
            driver.find_element_by_xpath("(//button[@type='button'])[3]").click()
        elif type_number == 2 or 3:
            driver.find_element_by_xpath("/html/body/main/div/div/md-card/md-card-content/form/ng-include/div/div[1]/md-input-container[2]/md-select/md-select-value").click()
            time.sleep(0.3)
            driver.find_element_by_xpath("/html/body/div[3]/md-select-menu/md-content/md-option[%s]" % random.randint(1, 3)).click()
            time.sleep(0.3)
            driver.find_element_by_xpath("//*[@ng-model=\"action.company\"]").click()
            time.sleep(0.3)
            driver.find_element_by_xpath('/html/body/div[4]/md-select-menu/md-content/md-option[%s]' % random.randint(1, 3)).click()
            time.sleep(0.3)
            driver.find_element_by_xpath("//*[@ng-model=\"action.contact\"]").click()
            time.sleep(0.3)
            driver.find_element_by_xpath('/html/body/div[5]/md-select-menu/md-content/md-option[%s]' % random.randint(1, 3)).click()
            time.sleep(0.3)
            driver.find_element_by_xpath('/html/body/main/div/div/md-card/md-card-content/form/ng-include/div/div[3]/md-input-container[1]/md-select/md-select-value').click()
            time.sleep(0.3)
            driver.find_element_by_xpath("/html/body/div[6]/md-select-menu/md-content/md-option[%s]" % random.randint(1, 2)).click()
            time.sleep(0.3)
            driver.find_element_by_xpath("/html/body/main/div/div/md-card/md-card-content/form/ng-include/div/div[3]/md-input-container[2]/div[1]/textarea").send_keys((u"Встреча №%s" % i))
            time.sleep(0.3)
            driver.find_element_by_xpath("(//button[@type='button'])[3]").click()

make_action(driver)

driver.quit()
