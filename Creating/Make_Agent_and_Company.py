# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import unittest, time, re
import random
import string

Company_amount = 30


driver = webdriver.Chrome()
driver.implicitly_wait(30)
base_url = "https://vkmt-test.lenoblgis.ru/"
driver.maximize_window() # max browser window size
verificationErrors = []
accept_next_alert = True

def make_company_and_contacts(driver):

    driver.get(base_url + "/#/login")
    driver.find_element_by_xpath("//*[@ng-model=\"login.username\"]").send_keys("User51")
    driver.find_element_by_xpath("//*[@ng-model=\"login.password\"]").send_keys("123")
    driver.find_element_by_xpath("//button[@type='button']").click()
    for i in range(Company_amount):
        company_name = 'Company#%s' % i
        company_adress = '%s street, %s' % (company_name, random.randint(100, 999))
        Name = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(8))
        Second_name = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(8))
        Third_name = ''.join(random.choice('abcdefghijklmnopqrstuvwxyz') for _ in range(8))
        Adress = '%s, %s' % (Name, random.randint(1, 999))
        x = random.randint(1, 6)
        # making company
        driver.find_element_by_xpath("/html/body/nav/div[2]/div[2]/div[2]/a").click()
        time.sleep(0.3)
        driver.find_element_by_css_selector("div.md-toolbar-tools > button.md-button.md-ink-ripple").click()
        driver.find_element_by_xpath("//*[@ng-model=\"company.name\"]").send_keys(company_name)
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"company.shortName\"]").send_keys(('Cmp#%s') % i)
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/form/md-dialog-content/md-tabs/md-tabs-content-wrapper/md-tab-content[1]/div/md-content/ng-include/div[2]/md-input-container[1]/md-select/md-select-value").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[4]/md-select-menu/md-content/md-option[%s]" % x).click()
        time.sleep(0.3)
        if x == 1:
            driver.find_element_by_xpath('/html/body/div[3]/md-dialog/form/md-dialog-content/md-tabs/md-tabs-content-wrapper/md-tab-content[1]/div/md-content/ng-include/div[2]/md-input-container[2]/md-select').click()
            time.sleep(0.3)
            driver.find_element_by_xpath('/html/body/div[5]/md-select-menu/md-content/md-optgroup[%s]/md-option[%s]' % (random.randint(1, 5), random.randint(1, 1))).click()
        driver.find_element_by_xpath("//*[@ng-model=\"company.phone\"]").send_keys(("+7 %s") % random.randint(1000000000, 9999999999))
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"company.fax\"]").send_keys(("+7 %s") % random.randint(1000000000, 9999999999))
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"company.email\"]").send_keys(("%s@mail.ru") % company_name)
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"company.site\"]").send_keys(("%s.ru") % (company_name))
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/form/md-dialog-content/md-tabs/md-tabs-wrapper/md-tabs-canvas/md-pagination-wrapper/md-tab-item[2]/span").click()
        time.sleep(0.8)
        driver.find_element_by_xpath("//*[@aria-label=\"Субъект\"]").click()
        time.sleep(0.3)
        driver.find_element_by_css_selector("md-autocomplete-parent-scope.ng-scope > span").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@aria-label=\"Район\"]").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/md-virtual-repeat-container[3]/div/div[2]/ul/li/md-autocomplete-parent-scope/span").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@aria-label=\"Город\"]").send_keys()
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/md-virtual-repeat-container[4]/div/div[2]/ul/li/md-autocomplete-parent-scope/span").click()
        driver.find_element_by_xpath("//*[@ng-model=\"company.address.street\"]").send_keys(random.randint(1, 99))
        driver.find_element_by_xpath("//*[@ng-model=\"company.address.house\"]").send_keys(random.randint(1, 99))
        driver.find_element_by_xpath("//*[@ng-model=\"company.address.building\"]").send_keys(random.randint(1, 99))
        driver.find_element_by_xpath("//*[@ng-model=\"company.address.housing\"]").send_keys(random.randint(1, 99))
        driver.find_element_by_xpath("//*[@ng-model=\"company.address.room\"]").send_keys(random.randint(1, 99))
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/form/md-dialog-actions/button[3]").click()
        time.sleep(1.3)
        # making Contact
        driver.find_element_by_xpath("/html/body/nav/div[2]/div[2]/div[3]/a").click()
        time.sleep(0.5)
        driver.find_element_by_css_selector("div.md-toolbar-tools > button.md-button.md-ink-ripple").click()
        driver.find_element_by_xpath("//*[@ng-model=\"contact.lastName\"]").send_keys(Name)
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.firstName\"]").send_keys(Second_name)
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.middleName\"]").send_keys(Third_name)
        time.sleep(0.3)
        driver.find_element_by_xpath('html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container[1]/md-select').click()
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[4]/md-select-menu/md-content/md-option[%s]" % random.randint(1, i+1)).click()
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container[2]/input").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.position\"]").send_keys(u"Директор")
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.personalPhone\"]").send_keys(("+7 %s") % random.randint(1000000000, 9999999999))
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.officialPhone\"]").send_keys(("+7 %s") % random.randint(1000000000, 9999999999))
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.email\"]").send_keys("%s@mail.ru" % Name)
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.address\"]").send_keys(Adress)
        time.sleep(0.3)
        driver.find_element_by_xpath("(//button[@type='button'])[8]").click()
        time.sleep(1)

make_company_and_contacts(driver)

driver.quit()
