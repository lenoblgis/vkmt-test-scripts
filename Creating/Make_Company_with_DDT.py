# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common import action_chains, keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
import xlrd, random

driver = webdriver.Chrome()
driver.implicitly_wait(5)
base_url = "https://vkmt-test.lenoblgis.ru"
driver.maximize_window() # max browser window size
verificationErrors = []
accept_next_alert = True
action = action_chains.ActionChains(driver)

def test_Company(driver):
    driver.get(base_url + "/#/login")
    driver.find_element_by_xpath("//*[@ng-model=\"login.username\"]").send_keys("Admin")
    driver.find_element_by_xpath("//*[@ng-model=\"login.password\"]").send_keys("123")
    driver.find_element_by_xpath("//button[@type='button']").click()
    wb=xlrd.open_workbook('C:\Users\smirnov\Tests\Test.xlsx')
    sh1 = wb.sheet_by_index(0)
    sh2 = wb.sheet_by_index(1)
    sh3 = wb.sheet_by_index(2)
    time.sleep(0.3)
    for i in range(2):
        rownum=(i+1)
        rows = sh1.row_values(rownum)
        driver.find_element_by_xpath('/html/body/nav/div[2]/div[2]/a[1]').click()
        time.sleep(0.3)
        driver.find_element_by_xpath('/html/body/md-content/md-card/md-toolbar/div/button').click()
        time.sleep(0.3)
        driver.find_element_by_xpath('//*[@ng-model=\"company.name\"]').send_keys(rows[0])
        time.sleep(0.3)
        driver.find_element_by_xpath('//*[@ng-model=\"company.shortName\"]').send_keys(rows[1])
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/form/md-dialog-content/md-tabs/md-tabs-content-wrapper/md-tab-content[1]/div/md-content/ng-include/div[2]/md-input-container[1]/md-select/md-select-value").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[4]/md-select-menu/md-content/md-option[%s]" % rows[3]).click()
        time.sleep(0.3)
        if rows[3] == 1:
            driver.find_element_by_xpath('/html/body/div[3]/md-dialog/form/md-dialog-content/md-tabs/md-tabs-content-wrapper/md-tab-content[1]/div/md-content/ng-include/div[2]/md-input-container[2]/md-select').click()
            time.sleep(0.3)
            driver.find_element_by_xpath('/html/body/div[5]/md-select-menu/md-content/md-optgroup[%s]/md-option[%s]' % (rows[5], rows[6])).click()
        driver.find_element_by_xpath("//*[@ng-model=\"company.phone\"]").send_keys(rows[7])
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"company.fax\"]").send_keys(rows[8])
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"company.email\"]").send_keys(rows[9])
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"company.site\"]").send_keys(rows[10])
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/form/md-dialog-content/md-tabs/md-tabs-wrapper/md-tabs-canvas/md-pagination-wrapper/md-tab-item[2]/span").click()
        time.sleep(0.8)
        driver.find_element_by_xpath("//*[@aria-label=\"Субъект\"]").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@aria-label=\"Субъект\"]").send_keys(rows[11])
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@aria-label=\"Район\"]").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@aria-label=\"Район\"]").send_keys(rows[12])
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@aria-label=\"Город\"]").send_keys()
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@aria-label=\"Город\"]").send_keys(rows[13])
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"company.address.street\"]").send_keys(rows[14])
        if not rows[15]:
            driver.find_element_by_xpath("//*[@ng-model=\"company.address.house\"]").send_keys(rows[15])
        else:
            driver.find_element_by_xpath("//*[@ng-model=\"company.address.house\"]").send_keys(int(rows[15]))

        if not rows[16]:
            driver.find_element_by_xpath("//*[@ng-model=\"company.address.building\"]").send_keys(rows[16])
        else:
            driver.find_element_by_xpath("//*[@ng-model=\"company.address.building\"]").send_keys(int(rows[16]))

        if not rows[17]:
            driver.find_element_by_xpath("//*[@ng-model=\"company.address.housing\"]").send_keys(rows[17])
        else:
            driver.find_element_by_xpath("//*[@ng-model=\"company.address.housing\"]").send_keys(int(rows[17]))

        if not rows[18]:
            driver.find_element_by_xpath("//*[@ng-model=\"company.address.room\"]").send_keys(rows[18])
        else:
            driver.find_element_by_xpath("//*[@ng-model=\"company.address.room\"]").send_keys(int(rows[18]))

        time.sleep(0.5)
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/form/md-dialog-actions/button[3]").click()
        time.sleep(1)




        # adding contact
        if rownum == 1:
            for u in range(4):
                rownum=(u+1)
                rows2 = sh2.row_values(rownum)
                time.sleep(1)
                driver.find_element_by_xpath('/html/body/md-content/div/div[2]/md-tabs/md-tabs-wrapper/md-tabs-canvas/md-pagination-wrapper/md-tab-item[2]/span').click()
                time.sleep(0.3)
                driver.find_element_by_xpath('/html/body/md-content/div/div[2]/md-tabs/md-tabs-content-wrapper/md-tab-content[2]/div/ng-include/md-card/md-card-actions/button').click()
                time.sleep(0.3)
                driver.find_element_by_xpath("//*[@ng-model=\"contact.lastName\"]").send_keys(rows2[0])
                time.sleep(0.3)
                driver.find_element_by_xpath("//*[@ng-model=\"contact.firstName\"]").send_keys(rows2[1])
                time.sleep(0.3)
                driver.find_element_by_xpath("//*[@ng-model=\"contact.middleName\"]").send_keys(rows2[2])
                time.sleep(0.3)
                driver.find_element_by_xpath("//*[@ng-model=\"contact.position\"]").send_keys(rows2[3])
                time.sleep(0.3)
                driver.find_element_by_xpath("//*[@ng-model=\"contact.personalPhone\"]").send_keys(rows2[4])
                time.sleep(0.3)
                driver.find_element_by_xpath("//*[@ng-model=\"contact.officialPhone\"]").send_keys(rows2[5])
                time.sleep(0.3)
                driver.find_element_by_xpath("//*[@ng-model=\"contact.email\"]").send_keys(rows2[6])
                time.sleep(0.3)
                driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-actions/button[2]").click()
                time.sleep(1)
        else:
            rows3 = sh3.row_values(rownum-1)
            time.sleep(0.5)
            driver.find_element_by_xpath('/html/body/md-content/div/div[2]/md-tabs/md-tabs-wrapper/md-tabs-canvas/md-pagination-wrapper/md-tab-item[2]/span').click()
            time.sleep(0.3)
            driver.find_element_by_xpath('/html/body/md-content/div/div[2]/md-tabs/md-tabs-content-wrapper/md-tab-content[2]/div/ng-include/md-card/md-card-actions/button').click()
            time.sleep(0.3)
            driver.find_element_by_xpath("//*[@ng-model=\"contact.lastName\"]").send_keys(rows3[0])
            time.sleep(0.3)
            driver.find_element_by_xpath("//*[@ng-model=\"contact.firstName\"]").send_keys(rows3[1])
            time.sleep(0.3)
            driver.find_element_by_xpath("//*[@ng-model=\"contact.middleName\"]").send_keys(rows3[2])
            time.sleep(0.3)
            driver.find_element_by_xpath("//*[@ng-model=\"contact.position\"]").send_keys(rows3[3])
            time.sleep(0.3)
            driver.find_element_by_xpath("//*[@ng-model=\"contact.personalPhone\"]").send_keys(rows3[4])
            time.sleep(0.3)
            driver.find_element_by_xpath("//*[@ng-model=\"contact.officialPhone\"]").send_keys(rows3[5])
            time.sleep(0.3)
            driver.find_element_by_xpath("//*[@ng-model=\"contact.email\"]").send_keys(rows3[6])
            time.sleep(0.3)
            driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-actions/button[2]").click()
            time.sleep(1)

    driver.quit()

test_Company(driver)
