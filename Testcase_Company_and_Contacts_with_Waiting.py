# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class TestcaseCompanyAndContacts(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(10)
        self.base_url = "https://vkmt-test.lenoblgis.ru/"
        self.driver.maximize_window() # max browser window size
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_case_company_and_contacts(self):
        driver = self.driver
        driver.get(self.base_url + "/#/login")
        wait = WebDriverWait(driver, 10)
        driver.find_element_by_xpath("//*[@ng-model=\"login.username\"]").send_keys("User1")
        driver.find_element_by_xpath("//*[@ng-model=\"login.password\"]").send_keys("123")
        driver.find_element_by_xpath("//button[@type='button']").click()
        # creating company
        driver.find_element_by_link_text(u"Контрагенты").click()
        driver.find_element_by_css_selector("div.md-toolbar-tools > button.md-button.md-ink-ripple").click()
        driver.find_element_by_xpath("//*[@ng-model=\"company.name\"]").send_keys(u"АО \"ЛенОблГис\"")
        wait.until(EC.element_to_be_clickable((By.XPATH,'//*[@ng-model=\"company.shortName\"]')))
        driver.find_element_by_xpath("//*[@ng-model=\"company.shortName\"]").send_keys(u"АО \"ЛОГ\"")
        wait.until(EC.element_to_be_clickable((By.XPATH,'//*[@ng-model=\"company.companyType\"]')))
        driver.find_element_by_xpath("//*[@ng-model=\"company.companyType\"]").click()
        wait.until(EC.element_to_be_clickable((By.XPATH,'/html/body/div[4]/md-select-menu/md-content/md-option[1]')))
        driver.find_element_by_xpath("/html/body/div[4]/md-select-menu/md-content/md-option[1]").click()
        wait.until(EC.element_to_be_clickable((By.XPATH,'//*[@aria-label=\"Тип ЛПУ\"]')))
        driver.find_element_by_xpath('//*[@aria-label=\"Тип ЛПУ\"]').click()
        wait.until(EC.element_to_be_clickable((By.XPATH,'/html/body/div[5]/md-select-menu/md-content/md-optgroup[3]/md-option[5]')))
        driver.find_element_by_xpath('/html/body/div[5]/md-select-menu/md-content/md-optgroup[3]/md-option[5]').click()
        wait.until(EC.element_to_be_clickable((By.XPATH,'//*[@ng-model=\"company.phone\"]')))
        driver.find_element_by_xpath("//*[@ng-model=\"company.phone\"]").send_keys("+11111111111")
        wait.until(EC.element_to_be_clickable((By.XPATH,'//*[@ng-model=\"company.fax\"]')))
        driver.find_element_by_xpath("//*[@ng-model=\"company.fax\"]").send_keys("+11111111111")
        wait.until(EC.element_to_be_clickable((By.XPATH,'//*[@ng-model=\"company.email\"]')))
        driver.find_element_by_xpath("//*[@ng-model=\"company.email\"]").send_keys("log@mail.ru")
        wait.until(EC.element_to_be_clickable((By.XPATH,'//*[@ng-model=\"company.site\"]')))
        driver.find_element_by_xpath("//*[@ng-model=\"company.site\"]").send_keys("log.ru")
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/form/md-dialog-actions/button[2]").click()
        wait.until(EC.element_to_be_clickable((By.XPATH,'//*[@aria-label=\"Субъект\"]')))
        driver.find_element_by_xpath("//*[@aria-label=\"Субъект\"]").send_keys(u'Адыгея Респ')
        wait.until(EC.element_to_be_clickable((By.XPATH,'//*[@aria-label=\"Район\"]')))
        driver.find_element_by_xpath("//*[@aria-label=\"Район\"]").click()
        wait.until(EC.element_to_be_clickable((By.XPATH,'/html/body/md-virtual-repeat-container[3]/div/div[2]/ul/li/md-autocomplete-parent-scope/span')))
        driver.find_element_by_xpath("/html/body/md-virtual-repeat-container[3]/div/div[2]/ul/li/md-autocomplete-parent-scope/span").click()
        wait.until(EC.element_to_be_clickable((By.XPATH,'//*[@aria-label=\"Город\"]')))
        driver.find_element_by_xpath("//*[@aria-label=\"Город\"]").send_keys()
        wait.until(EC.element_to_be_clickable((By.XPATH,'/html/body/md-virtual-repeat-container[4]/div/div[2]/ul/li/md-autocomplete-parent-scope/span')))
        driver.find_element_by_xpath("/html/body/md-virtual-repeat-container[4]/div/div[2]/ul/li/md-autocomplete-parent-scope/span").click()
        driver.find_element_by_xpath("//*[@ng-model=\"company.address.street\"]").send_keys("11")
        driver.find_element_by_xpath("//*[@ng-model=\"company.address.house\"]").send_keys("22")
        driver.find_element_by_xpath("//*[@ng-model=\"company.address.building\"]").send_keys("33")
        driver.find_element_by_xpath("//*[@ng-model=\"company.address.housing\"]").send_keys("44")
        driver.find_element_by_xpath("//*[@ng-model=\"company.address.room\"]").send_keys("55")
        wait.until(EC.element_to_be_clickable((By.XPATH,'/html/body/div[3]/md-dialog/form/md-dialog-actions/button[3]')))
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/form/md-dialog-actions/button[3]").click()
        wait.until(EC.element_to_be_clickable((By.XPATH,'/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-title/md-card-title-text/span')))
        # checking company
        try: self.assertEqual(u"АО \"ЛОГ\"", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-title/md-card-title-text/span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Государственные ЛПУ", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[1]/p/span[1]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u", Диспансер, наркологический", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[1]/p/span[2]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"АО \"ЛенОблГис\"", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[2]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"+1 (111) 111-11-11 / +1 (111) 111-11-11", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[3]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"log@mail.ru", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[4]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"http://log.ru", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[5]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Адыгея Респ Кошехабльский район п Комсомольский улица 11 дом 22 сооружение 33 корпус 44 помещение 55", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[7]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_xpath("/html/body/main/div/div[2]/md-tabs/md-tabs-wrapper/md-tabs-canvas/md-pagination-wrapper/md-tab-item[3]/span").click()
        time.sleep(1.5)
        try: self.assertEqual(u"Смирнов Валентин создание card_travel АО \"ЛОГ\" (Государственные ЛПУ)", driver.find_element_by_xpath("/html/body/main/div/div[2]/md-tabs/md-tabs-content-wrapper/md-tab-content[3]/div/ng-include/md-card/md-list/md-list-item[1]/div[1]/h3").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        #editing company
        driver.find_element_by_xpath('/html/body/main/button[1]').click()
        time.sleep(0.5)
        driver.find_element_by_xpath("//*[@ng-model=\"company.name\"]").clear()
        driver.find_element_by_xpath("//*[@ng-model=\"company.name\"]").send_keys(u"ООО \"ОблЛенГИС\"")
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"company.shortName\"]").clear()
        driver.find_element_by_xpath("//*[@ng-model=\"company.shortName\"]").send_keys(u"ООО \"ОЛГ\"")
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"company.companyType\"]").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[4]/md-select-menu/md-content/md-option[2]").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"company.phone\"]").clear()
        driver.find_element_by_xpath("//*[@ng-model=\"company.phone\"]").send_keys("+2 (222) 222-22-22")
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"company.fax\"]").clear()
        driver.find_element_by_xpath("//*[@ng-model=\"company.fax\"]").send_keys("+2 (222) 222-22-22")
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"company.email\"]").clear()
        driver.find_element_by_xpath("//*[@ng-model=\"company.email\"]").send_keys("ogl-ogl@mail.ru")
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"company.site\"]").clear()
        driver.find_element_by_xpath("//*[@ng-model=\"company.site\"]").send_keys("ogl-ogl.ru")
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/form/md-dialog-content/md-tabs/md-tabs-wrapper/md-tabs-canvas/md-pagination-wrapper/md-tab-item[2]/span").click()
        time.sleep(0.8)
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/form/md-dialog-content/md-tabs/md-tabs-content-wrapper/md-tab-content[2]/div/md-content/ng-include/div/md-autocomplete[1]/md-autocomplete-wrap/button").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@aria-label=\"Субъект\"]").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@aria-label=\"Субъект\"]").send_keys(u'Алтай Респ')
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@aria-label=\"Район\"]").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@aria-label=\"Район\"]").send_keys(u'Майминский район')
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@aria-label=\"Город\"]").send_keys()
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@aria-label=\"Город\"]").send_keys(u'п Алферово')
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"company.address.street\"]").send_keys("111")
        driver.find_element_by_xpath("//*[@ng-model=\"company.address.house\"]").send_keys("222")
        driver.find_element_by_xpath("//*[@ng-model=\"company.address.building\"]").send_keys("333")
        driver.find_element_by_xpath("//*[@ng-model=\"company.address.housing\"]").send_keys("444")
        driver.find_element_by_xpath("//*[@ng-model=\"company.address.room\"]").send_keys("555")
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/form/md-dialog-actions/button[3]").click()
        time.sleep(0.5)
        #cheking edit company
        try: self.assertEqual(u"ООО \"ОЛГ\"", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-title/md-card-title-text/span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Ведомственные ЛПУ", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[1]/p/span[1]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"ООО \"ОблЛенГИС\"", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[2]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"+2 (222) 222-22-22 / +2 (222) 222-22-22", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[3]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"ogl-ogl@mail.ru", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[4]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"http://ogl-ogl.ru", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[5]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Сибирский федеральный округ", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[6]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Алтай Респ Майминский район п Алферово улица 11111 дом 22222 сооружение 33333 корпус 44444 помещение 55555", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[7]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        time.sleep(0.5)
        driver.find_element_by_xpath("/html/body/main/div/div[2]/md-tabs/md-tabs-wrapper/md-tabs-canvas/md-pagination-wrapper/md-tab-item[3]/span").click()
        time.sleep(1)
        try: self.assertEqual(u"Смирнов Валентин редактирование card_travel ООО \"ОЛГ\" (Ведомственные ЛПУ)", driver.find_element_by_xpath("/html/body/main/div/div[2]/md-tabs/md-tabs-content-wrapper/md-tab-content[3]/div/ng-include/md-card/md-list/md-list-item[1]/div[1]/h3").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        #get old version
        driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[2]/md-card/md-list/div[2]/md-list-item[2]/div/div[2]/button[1]").click()
        time.sleep(0.3)
        #cheking old version
        try: self.assertEqual(u"АО \"ЛОГ\"", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-title/md-card-title-text/span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Государственные ЛПУ", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[1]/p/span[1]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u", Диспансер, наркологический", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[1]/p/span[2]").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"АО \"ЛенОблГис\"", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[2]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"+1 (111) 111-11-11 / +1 (111) 111-11-11", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[3]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"log@mail.ru", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[4]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"http://log.ru", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[5]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Адыгея Респ Кошехабльский район п Комсомольский улица 11 дом 22 сооружение 33 корпус 44 помещение 55", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[7]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        #creating contact
        time.sleep(0.5)
        driver.find_element_by_xpath("/html/body/main/div/div[2]/md-tabs/md-tabs-wrapper/md-tabs-canvas/md-pagination-wrapper/md-tab-item[2]/span").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/main/div/div[2]/md-tabs/md-tabs-content-wrapper/md-tab-content[2]/div/ng-include/md-card/md-card-actions/button").click()
        time.sleep(0.3)
        driver.find_element_by_xpath('//*[@ng-model="contact.lastName"]').send_keys(u'Иванов')
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.firstName\"]").send_keys(u'Иван')
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.middleName\"]").send_keys(u'Иванович')
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.position\"]").send_keys(u"Директор")
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.personalPhone\"]").send_keys("+7 (812) 555 55 55")
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.officialPhone\"]").send_keys("+7 (812) 555 55 55")
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.email\"]").send_keys("mail@mail.ru")
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-actions/button[2]").click()
        time.sleep(1)
        #cheking contact
        driver.find_element_by_xpath("/html/body/main/div/div[2]/md-tabs/md-tabs-content-wrapper/md-tab-content[2]/div/ng-include/md-card/md-list/md-list-item[1]/div/button").click()
        time.sleep(0.5)
        try: self.assertEqual(u"Иванов Иван Иванович", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-title/md-card-title-text/span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"АО \"ЛенОблГис\"", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[1]/p/a/span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Директор", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[2]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"+7 (812) 555-55-55", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[3]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"+7 (812) 555-55-55", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[4]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"mail@mail.ru", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[5]/p/a/span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Адыгея Респ, п Комсомольский, улица 11, дом 22, сооружение 33, корпус 44, помещение 55", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[6]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_xpath("/html/body/main/div/div[2]/md-tabs/md-tabs-wrapper/md-tabs-canvas/md-pagination-wrapper/md-tab-item[2]/span").click()
        time.sleep(0.3)
        try: self.assertEqual(u"Смирнов Валентин создание account_box Иванов Иван Иванович (Директор)", driver.find_element_by_xpath("/html/body/main/div/div[2]/md-tabs/md-tabs-content-wrapper/md-tab-content[2]/div/ng-include/md-card/md-list/md-list-item[1]/div[1]/h3").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        #editing contact
        driver.find_element_by_xpath("/html/body/main/button[1]").click()
        time.sleep(0.3)
        driver.find_element_by_xpath('//*[@ng-model="contact.lastName"]').clear()
        driver.find_element_by_xpath('//*[@ng-model="contact.lastName"]').send_keys(u'Петров')
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.firstName\"]").clear()
        driver.find_element_by_xpath("//*[@ng-model=\"contact.firstName\"]").send_keys(u'Петр')
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.middleName\"]").clear()
        driver.find_element_by_xpath("//*[@ng-model=\"contact.middleName\"]").send_keys(u'Петрович')
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[2]/md-input-container[1]/md-select/md-select-value/span[1]").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[4]/md-select-menu/md-content/md-option[1]/div").click()
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.position\"]").clear()
        driver.find_element_by_xpath("//*[@ng-model=\"contact.position\"]").send_keys(u"Менеджер")
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.personalPhone\"]").clear()
        driver.find_element_by_xpath("//*[@ng-model=\"contact.personalPhone\"]").send_keys("+7 (812) 666 66 66")
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.officialPhone\"]").clear()
        driver.find_element_by_xpath("//*[@ng-model=\"contact.officialPhone\"]").send_keys("+7 (812) 666 66 66")
        time.sleep(0.3)
        driver.find_element_by_xpath("//*[@ng-model=\"contact.email\"]").clear()
        driver.find_element_by_xpath("//*[@ng-model=\"contact.email\"]").send_keys("petrov@mail.ru")
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[4]/md-input-container/div[1]/textarea").clear()
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div[4]/md-input-container/div[1]/textarea").send_keys("Just adress, 75")
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-actions/button[2]").click()
        time.sleep(1)
        #cheking edit contact
        try: self.assertEqual(u"Петров Петр Петрович", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-title/md-card-title-text/span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Менеджер", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[2]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"+7 (812) 666-66-66", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[3]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"+7 (812) 666-66-66", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[4]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"petrov@mail.ru", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[5]/p/a/span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Just adress, 75", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[6]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_xpath("/html/body/main/div/div[2]/md-tabs/md-tabs-wrapper/md-tabs-canvas/md-pagination-wrapper/md-tab-item[2]/span").click()
        time.sleep(0.3)
        try: self.assertEqual(u"Смирнов Валентин редактирование account_box Петров Петр Петрович (Менеджер)", driver.find_element_by_xpath("/html/body/main/div/div[2]/md-tabs/md-tabs-content-wrapper/md-tab-content[2]/div/ng-include/md-card/md-list/md-list-item[1]/div[1]/h3").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        #get old version
        driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[2]/md-card/md-list/div[2]/md-list-item[2]/div/div[2]/button[1]").click()
        time.sleep(0.3)
        #cheking old version
        time.sleep(0.5)
        try: self.assertEqual(u"Иванов Иван Иванович", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-title/md-card-title-text/span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"АО \"ЛенОблГис\"", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[1]/p/a/span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Директор", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[2]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"+7 (812) 555-55-55", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[3]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"+7 (812) 555-55-55", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[4]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"mail@mail.ru", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[5]/p/a/span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Адыгея Респ, п Комсомольский, улица 11, дом 22, сооружение 33, корпус 44, помещение 55", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[6]/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        #creating action
        time.sleep(0.5)
        driver.find_element_by_xpath("/html/body/main/button[3]").click()
        time.sleep(0.5)
        driver.find_element_by_xpath('/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div/div[3]/md-input-container[2]/div[1]/textarea').click()
        time.sleep(0.5)
        driver.find_element_by_xpath('/html/body/div[3]/md-dialog/md-dialog-content/form/ng-include/div/div[3]/md-input-container[2]/div[1]/textarea').send_keys(u'Важная встреча с покупателем')
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-actions/button[2]").click()
        time.sleep(0.5)
        #cheking action
        try: self.assertEqual(u"Важная встреча с покупателем", driver.find_element_by_xpath("/html/body/main/div/div[2]/md-tabs/md-tabs-content-wrapper/md-tab-content[1]/div/ng-include/md-card/md-card-content/p").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        time.sleep(0.3)
        driver.find_element_by_xpath('/html/body/main/div/div[2]/md-tabs/md-tabs-content-wrapper/md-tab-content[1]/div/ng-include/md-card/md-card-actions/a/span').click()
        time.sleep(0.5)
        try: self.assertEqual(u"Смирнов Валентин создание playlist_add_check Встреча : Важная встреча с покупателем", driver.find_element_by_xpath("/html/body/main/div/div[2]/ng-include/md-card/md-list/md-list-item[1]/div[1]/h3").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        time.sleep(0.3)
        #edit action
        driver.find_element_by_xpath('/html/body/main/div/div[1]/md-card/md-card-content/form/ng-include/div/div[1]/md-input-container[1]/md-select/md-select-value/span[1]/div').click()
        time.sleep(0.5)
        driver.find_element_by_xpath('/html/body/div[3]/md-select-menu/md-content/md-option[2]').click()
        time.sleep(0.3)
        driver.find_element_by_xpath('/html/body/main/div/div[1]/md-card/md-card-content/form/ng-include/div/div[1]/md-input-container[2]/md-select/md-select-value/span[1]').click()
        time.sleep(0.5)
        driver.find_element_by_xpath('/html/body/div[4]/md-select-menu/md-content/md-option[3]/div').click()
        time.sleep(0.5)
        driver.find_element_by_xpath('/html/body/main/div/div[1]/md-card/md-card-content/form/ng-include/div/div[3]/md-input-container[1]/md-select/md-select-value/span[1]/div').click()
        time.sleep(0.3)
        driver.find_element_by_xpath('/html/body/div[5]/md-select-menu/md-content/md-option[1]/div').click()
        time.sleep(0.3)
        driver.find_element_by_xpath('/html/body/main/div/div[1]/md-card/md-card-content/form/ng-include/div/div[3]/md-input-container[2]/div[1]/textarea').clear()
        driver.find_element_by_xpath('/html/body/main/div/div[1]/md-card/md-card-content/form/ng-include/div/div[3]/md-input-container[2]/div[1]/textarea').send_keys(u'Очень важная встреча с покупателем')
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/main/div/div[1]/md-card/md-card-action/md-card-action/button[2]").click()
        time.sleep(0.5)
        #cheking edited action
        try: self.assertEqual(u"Смирнов Валентин редактирование playlist_add_check Звонок : Очень важная встреча с покупателем", driver.find_element_by_xpath("/html/body/main/div/div[2]/ng-include/md-card/md-list/md-list-item[1]/div[1]/h3").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        #cheking links in action
        time.sleep(0.5)
        driver.find_element_by_xpath("/html/body/main/div/div[1]/md-card/md-card-action/a/span").click()
        time.sleep(0.5)
        driver.find_element_by_xpath("/html/body/main/div/div/ng-include/md-card[1]/md-card-content/p[2]/span/a").click()
        time.sleep(0.5)
        try: self.assertEqual(u"Иванов Иван Иванович", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-title/md-card-title-text/span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        time.sleep(0.5)
        driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-content/ul/li[1]/p/a/span").click()
        time.sleep(0.5)
        try: self.assertEqual(u"АО \"ЛОГ\"", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include[1]/md-card/md-card-title/md-card-title-text/span").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        #cheking activity
        driver.find_element_by_xpath("/html/body/nav/div[2]/div[1]/div/a").click()
        time.sleep(0.5)
        try: self.assertEqual(u"Смирнов Валентин редактирование playlist_add_check Звонок : Очень важная встреча с покупателем", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include/md-card/md-list/md-list-item[1]/div[1]/h3").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Смирнов Валентин создание playlist_add_check Встреча : Важная встреча с покупателем", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include/md-card/md-list/md-list-item[2]/div[1]/h3").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Смирнов Валентин редактирование account_box Петров Петр Петрович (Менеджер)", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include/md-card/md-list/md-list-item[3]/div[1]/h3").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Смирнов Валентин создание account_box Иванов Иван Иванович (Директор)", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include/md-card/md-list/md-list-item[4]/div[1]/h3").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Смирнов Валентин редактирование card_travel ООО \"ОЛГ\" (Ведомственные ЛПУ)", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include/md-card/md-list/md-list-item[5]/div[1]/h3").text)
        except AssertionError as e: self.verificationErrors.append(str(e))
        try: self.assertEqual(u"Смирнов Валентин создание card_travel АО \"ЛОГ\" (Государственные ЛПУ)", driver.find_element_by_xpath("/html/body/main/div/div[1]/ng-include/md-card/md-list/md-list-item[6]/div[1]/h3").text)
        except AssertionError as e: self.verificationErrors.append(str(e))


    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        if sys.exc_info()[0]:  # Returns the info of exception being handled
            fail_url = self.driver.current_url
            print fail_url
            now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S-%f')
            self.driver.get_screenshot_as_file('C:\Users\smirnov\Desktop\%s.png' % now) # my tests work in parallel, so I need uniqe file names
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
