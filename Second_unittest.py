import unittest

def add(a, b, c=0):
    return (a+b+c)

class AddTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_positive_add(self):
        self.assertEqual(add(10,23,22), 55) # Test will fail if "false" (boolean)
        self.assertEqual(add(11,23), 34)
        self.assertEqual(add(1,1,17), 19)


    def test_negative_add(self):
        self.assertEqual(add(-12,23), 11)

if __name__=='__main__':
   unittest.main()
