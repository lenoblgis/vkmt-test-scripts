# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class LogIn(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(30)
        self.base_url = "https://vkmt-test.lenoblgis.ru/"
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_log_in(self):
        driver = self.driver
        driver.get(self.base_url + "/#/login")
        driver.find_element_by_xpath("/html/body/main/div/md-card/md-card-content/form/md-input-container[1]/input").click()
        driver.find_element_by_xpath("/html/body/main/div/md-card/md-card-content/form/md-input-container[1]/input").clear()
        driver.find_element_by_xpath("/html/body/main/div/md-card/md-card-content/form/md-input-container[1]/input").send_keys("admin")
        driver.find_element_by_xpath("/html/body/main/div/md-card/md-card-content/form/md-input-container[2]/input").clear()
        driver.find_element_by_xpath("/html/body/main/div/md-card/md-card-content/form/md-input-container[2]/input").send_keys("123")
        driver.find_element_by_xpath("//button[@type='button']").click()

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
