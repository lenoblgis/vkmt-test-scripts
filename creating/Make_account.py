# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import unittest, time, re
import xlrd, random

binary = FirefoxBinary(r"C:\Program Files (x86)\Mozilla Firefox\firefox.exe")
wb=xlrd.open_workbook('C:\Users\smirnov\Tests\Make_account_DDT.xlsx')
sh1 = wb.sheet_by_index(0)

Account_amount = 2


driver = webdriver.Firefox(firefox_binary=binary)
driver.implicitly_wait(10)
base_url = "https://vkmt-test.lenoblgis.ru/"
driver.maximize_window() # max browser window size
verificationErrors = []
accept_next_alert = True


def make_account(driver):

    driver.get(base_url + "/#/accounts")
    rownum = 1
    rows = sh1.row_values(rownum)
    driver.find_element_by_xpath("//*[@ng-model=\"login.username\"]").send_keys("Admin")
    driver.find_element_by_xpath("//*[@ng-model=\"login.password\"]").send_keys("123")
    driver.find_element_by_xpath("//button[@type='button']").click()
    time.sleep(0.3)
    driver.find_element_by_link_text(u"Учетные записи").click()
    time.sleep(0.3)
    driver.find_element_by_xpath("/html/body/main/md-card/md-table-container/table/tbody/tr[1]/td[1]").click()
    time.sleep(0.3)
    driver.find_element_by_xpath("/html/body/div[2]/div/div/div[1]/a/md-icon").click()
    time.sleep(0.5)
    driver.find_element_by_xpath('//*[@ng-model="account.lastName"]').clear()
    driver.find_element_by_xpath('//*[@ng-model="account.lastName"]').send_keys(rows[2])
    driver.find_element_by_xpath('//*[@ng-model="account.firstName"]').clear()
    driver.find_element_by_xpath('//*[@ng-model="account.firstName"]').send_keys(rows[3])
    driver.find_element_by_xpath('//*[@ng-model="account.position"]').clear()
    driver.find_element_by_xpath('//*[@ng-model="account.position"]').send_keys(rows[7])
    time.sleep(0.3)
    driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-actions/button[2]").click()
    for i in range(Account_amount):
        rownum=(i+2)
        rows = sh1.row_values(rownum)
        time.sleep(2)
        driver.find_element_by_xpath("/html/body/nav/div[2]/div[4]/div[2]").click()
        time.sleep(1)
        driver.find_element_by_xpath("/html/body/main/md-card/md-toolbar/div/button").click()
        time.sleep(1)
        driver.find_element_by_xpath("//*[@ng-model=\"account.login\"]").send_keys(rows[0])
        time.sleep(0.3)
        driver.find_element_by_xpath('//*[@ng-model=\"account.password\"]').send_keys(int(rows[1]))
        time.sleep(0.3)
        driver.find_element_by_xpath('//*[@ng-model=\"account.lastName\"]').send_keys(rows[2])
        time.sleep(0.3)
        driver.find_element_by_xpath('//*[@ng-model=\"account.firstName\"]').send_keys(rows[3])
        time.sleep(0.3)
        driver.find_element_by_xpath('//*[@ng-model=\"account.middleName\"]').send_keys(rows[4])
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-actions/button[2]").click()
        time.sleep(0.8)
        driver.find_element_by_xpath("/html/body/main/button[1]").click()
        time.sleep(0.8)
        driver.find_element_by_xpath('//*[@ng-model="account.email"]').send_keys(rows[5])
        time.sleep(0.3)
        driver.find_element_by_xpath('//*[@ng-model="account.phone"]').send_keys(rows[6])
        time.sleep(0.3)
        driver.find_element_by_xpath('//*[@ng-model="account.position"]').send_keys(rows[7])
        time.sleep(0.3)
        driver.find_element_by_xpath("/html/body/div[3]/md-dialog/md-dialog-actions/button[2]").click()


    driver.quit()


make_account(driver)
